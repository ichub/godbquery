package baseconfig

import "gitee.com/ichub/godbquery/common/base/baseconsts"

type DbClientDto struct {
	IchubClientDto

	Dbtype string `json:"dbtype"`
	Dbname string `json:"dbname"`
	Host   string `json:"host"`
	Port   string `json:"port"`

	Username string `json:"username"`
	Password string `json:"password"`
	Sslmode  string `json:"sslmode"`
	Charset  string `json:"charset"`

	GormClient *GormClientDto `json:"gorm_client"`
}

func NewDbClientDto() *DbClientDto {
	var ds = &DbClientDto{}
	ds.InitProxy(ds)
	return ds
}
func (this *DbClientDto) IsMysql() bool {
	return this.Dbtype == baseconsts.DB_TYPE_MYSQL

}
