package baseconfig

type FactroyClientDto struct {
	Author   string
	PathNew  string
	PathShop string
	PathNow  string
	PathTest string
}
