package baseconfig

type GormClientDto struct {
	IchubClientDto `json:"-"`

	Debug  string `json:"debug"`
	DbType string `json:"db_type"`
	Enable string `json:"enabled"`

	MaxLifetime  string `json:"max_lifetime"`
	MaxOpenConns string `json:"max_open_conns"`
	MaxIdleConns string `json:"max_idle_conns"`
}

func NewGormClientDto() *GormClientDto {
	var dto = &GormClientDto{}
	dto.InitProxy(dto)
	return dto
}
