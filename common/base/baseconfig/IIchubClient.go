package baseconfig

type IIchubClient interface {
	ParseValue(key, value string) string
	CopyWithOption(from any)
	Parse() IIchubClient
}
