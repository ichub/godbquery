package baseconfig

import (
	"gitee.com/ichub/godbquery/common/base/basedto"
	"github.com/morrisxyang/xreflect"
	"github.com/sirupsen/logrus"
	"reflect"
)

type IchubClientDto struct {
	basedto.BaseEntity
}

func (this *IchubClientDto) ParseValue(key, value string) string {
	item := &ConfigItem{
		Key:   key,
		Value: value,
	}
	return item.ParseValue().EndValue
}
func (this *IchubClientDto) Parse() IIchubClient {
	if this.Proxy().Some == nil {
		logrus.Error("some is nil!")
		return nil
	}
	Values, _ := xreflect.Fields(this.Proxy().Some)
	for k, v := range Values {

		if v.Kind() == reflect.String {

			var vv = this.ParseValue(k, v.String())
			v.SetString(vv)

			xreflect.SetField(this, k, v)
		} else if v.Kind() == reflect.Struct {
			this.ParseStruct(v)
		}

	}
	return this.Proxy().Some.(IIchubClient)
}
func (this *IchubClientDto) ParseStruct(value reflect.Value) {

	Values, _ := xreflect.Fields(value)
	for k, v := range Values {

		if v.Kind() == reflect.String {

			var vv = this.ParseValue(k, v.String())
			v.SetString(vv)

			xreflect.SetField(value, k, v)
		}
		if v.Kind() == reflect.Struct {
			this.ParseStruct(v)
		}

	}
}

func (this *IchubClientDto) ParseValues(key string, values ...*string) {

	for _, v := range values {

		*v = this.ParseValue("key", *v)
	}
	logrus.Info(values)
}
