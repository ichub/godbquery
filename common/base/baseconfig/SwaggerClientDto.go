package baseconfig

type SwaggerClientDto struct {
	IchubClientDto `json:"-"`
	Host           string `json:"host"`
	BasePath       string `json:"base_path"`
	Version        string `json:"version"`
	Title          string `json:"title"`
	Enable         string `json:"enable"`
}

func NewSwaggerClientDto() *SwaggerClientDto {
	var dto = &SwaggerClientDto{}
	dto.InitProxy(dto)
	return dto
}
