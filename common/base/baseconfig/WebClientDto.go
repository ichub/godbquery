package baseconfig

/*
@Title    文件名称: WebServerDto.go
@Description  描述: Web服务配置信息

@Author  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
*/
type WebClientDto struct {
	//bServerDto `json:"web_server_dto"`
	//used by client
	EtcdHost   string `json:"etcd_host"`
	ServerName string `json:"server_name"`
	TestUrl    string `json:"test_url"`
	IchubClientDto
}

func NewWebClientDto() *WebClientDto {
	return &WebClientDto{}
}

//func (this *WebClientDto) Parse() *WebClientDto {
//	//is.WebServerDto.Parse()
//	this.EtcdHost = this.ParseValue("EtcdHost", this.EtcdHost)
//	this.ServerName = this.ParseValue("ServerName", this.ServerName)
//	this.TestUrl = this.ParseValue("TestUrl", this.TestUrl)
//
//	return this
//}
