package basedao

import (
	"gitee.com/ichub/godbquery/common/base/basemodel"
	"gitee.com/ichub/godbquery/common/dbcontent"
	"github.com/jinzhu/gorm"
	"time"
)

type BaseDao struct {
}

func (this *BaseDao) GetDB() *gorm.DB {
	return dbcontent.GetDB()
}

func (this *BaseDao) formatTime(sec int64) string {
	timeLayout := "2006-01-02 15:04:05"
	datetime := time.Unix(sec, 0).Format(timeLayout)
	return datetime
}

func (this *BaseDao) formatDate(sec int64) string {
	timeLayout := "2006-01-02"
	datetime := time.Unix(sec, 0).Format(timeLayout)
	return datetime
}

func (this *BaseDao) localTimeFormat(ptime time.Time) string {
	timeLayout := "2006-01-02 15:04:05"
	return ptime.Format(timeLayout)

}

func (this *BaseDao) localTimeUTCFormat(ptime time.Time) string {
	timeLayout := "2006-01-02 15:04:05"
	return ptime.UTC().Format(timeLayout)

}

func (this *BaseDao) localDateFormat(localDateInt basemodel.LocalDateInt) string {
	timeLayout := "2006-01-02"
	datetime := localDateInt.Time.Format(timeLayout)
	return datetime
}

func (this *BaseDao) localDatetimeFormat(localTimeInt basemodel.LocalTimeInt) string {
	timeLayout := "2006-01-02 15:04:05"
	datetime := localTimeInt.Time.Format(timeLayout)
	return datetime
}
