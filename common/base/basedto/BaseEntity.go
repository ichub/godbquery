package basedto

import (
	"gitee.com/ichub/godbquery/common/base/baseutils"
	"gitee.com/ichub/godbquery/common/base/baseutils/stringutils"
)

type BaseEntity struct {
	Proxy func() *BaseProxy `json:"-"`
	// Id        *uint64     `gorm:"primary_key" json:"id"`
	//CreatedAt int64 `json:"created_at,omitempty"`
	//UpdatedAt int64 `json:"updated_at,omitempty"`
	//DeletedAt int64 `gorm:"column:deleted_at;type:timestamp" sql:"index" json:"-"`
	//CreatedBy int64       `json:"create_by,omitempty"`
	//UpdatedBy int64       `json:"updated_by,omitempty"`
	//DeletedBy int64       `json:"deleted_by,omitempty"`

}

func NewBaseEntity() *BaseEntity {
	var entity = &BaseEntity{}

	entity.InitProxy(entity)
	return entity
}

func (this *BaseEntity) InitProxy(some any) {
	this.Proxy = func() *BaseProxy {

		return NewBaseProxy(some)
	}

}

func (this *BaseEntity) String() string {

	return this.Proxy().String()
}

func (this *BaseEntity) ToString() string {

	return this.Proxy().ToString()
}

func (this *BaseEntity) Log() {

	this.Proxy().Log()

}

func (this *BaseEntity) Clone() interface{} {
	return this.Proxy().Clone()
}

func (this *BaseEntity) ValueOf(that any) {
	this.Proxy().ValueOf(that)
}

func (this *BaseEntity) FromJson(body []byte) interface{} {
	return this.Proxy().FromJson(body)

}
func (this *BaseEntity) ToJson() string {
	return this.Proxy().ToJson()

}
func (this *BaseEntity) ToPrettyString() string {
	if this.Proxy() == nil {
		return "{}"
	}
	return this.Proxy().ToPrettyString()
}

func (this *BaseEntity) Id2Str(id int64) string {
	return baseutils.Any2Str(id)
}

func (this *BaseEntity) ValueFrom(from any) {
	this.Proxy().ValueFrom(from)

}

func (this *BaseEntity) Str2Int(from string) int {

	return stringutils.Str2Int(from)
}
func (this *BaseEntity) CopyWithOption(from any) {
	this.Proxy().CopyWithOption(from)

}
