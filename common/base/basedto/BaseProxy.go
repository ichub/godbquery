package basedto

import (
	"gitee.com/ichub/godbquery/common/base/baseutils/jsonutils"
	"github.com/jinzhu/copier"
	"github.com/sirupsen/logrus"
	"reflect"
)

type BaseProxy struct {
	Some any
}

func NewBaseProxy(some any) *BaseProxy {
	var proxy = new(BaseProxy)
	proxy.Some = some
	return proxy
}

func (this *BaseProxy) String() string {

	return jsonutils.ToJsonPretty(this.Some)
}

func (this *BaseProxy) ToString() string {

	return jsonutils.ToJsonPretty(this.Some)
}

func (this *BaseProxy) Log() {
	var name = reflect.TypeOf(this.Some).String()
	logrus.Info(name, " =", this.String())

}

func (this *BaseProxy) Clone() any {

	return nil //clone.Clone(this.Some)

}

func (this *BaseProxy) FromJson(body []byte) interface{} {
	jsonutils.FromJsonByte(body, this.Some)
	return this.Some

}
func (this *BaseProxy) ToJson() string {
	return jsonutils.ToJsonPretty(this.Some)

}

func (this *BaseProxy) ValueOf(another interface{}) {
	this.Some = another
}
func (this *BaseProxy) ToPrettyString() string {
	if this.Some == nil {
		return "{}"
	}
	return jsonutils.ToJsonPretty(this.Some)
}

func (this *BaseProxy) ValueFrom(from any) {
	//mapstructure.Decode(from, this.Some)
	var fromstr = jsonutils.ToJsonPretty(from)
	jsonutils.FromJson(fromstr, this.Some)

}

func (this *BaseProxy) CopyWithOption(from any) {
	copier.CopyWithOption(this.Some, from, copier.Option{IgnoreEmpty: true})

}
