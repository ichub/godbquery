package basedto

import (
	"encoding/json"
	"gitee.com/ichub/godbquery/common/base/baseutils"
	"gitee.com/ichub/godbquery/common/base/baseutils/jsonutils"
	"github.com/gookit/goutil/strutil"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"gopkg.in/resty.v1"
)

/*
	@Title    文件名称: ichubresult.go
	@Description  描述: 统一返回结构

	@Author  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
	@Update  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
*/

type IchubResult struct {
	BaseEntity

	//返回码
	Code int `json:"code"`
	//信息
	Msg string `json:"msg"`
	//数据
	Data any `json:"data,omitempty"`
}

func NewIchubResult() *IchubResult {
	var result = &IchubResult{
		Code: 200,
		Msg:  "成功",
	}
	result.InitProxy(result)
	return result

}

func ValueOf(resp *resty.Response) *IchubResult {
	var result IchubResult
	result.InitProxy(&result)
	if resp.StatusCode() != 200 {
		return result.CodeMsg(resp.StatusCode(), string(resp.Body()))
	}
	jsonutils.FromJsonByte(resp.Body(), &result)
	return &result
}
func ParseIchubResult(body []byte) *IchubResult {
	var result IchubResult
	jsonutils.FromJsonByte(body, &result)
	return &result
}
func (this *IchubResult) CheckCode(suite suite.Suite, code int) {

	suite.Equal(code, this.Code)
}

func (this *IchubResult) Check(suite suite.Suite, keyVal string) {
	var kv = strutil.Split(keyVal, "=")
	suite.Equal(baseutils.Any2Str(this.ValueByKey(kv[0])), kv[1])
}

// keyVal : "Pay=12|l=2"
func (this *IchubResult) Checks(suite suite.Suite, keyVals string) {
	kvs := strutil.Split(keyVals, "|")
	for _, keyval := range kvs {
		this.Check(suite, keyval)
	}

}

// func DataOfIndex(1)
func (this *IchubResult) FromJson(body []byte) (*IchubResult, error) {

	var err = json.Unmarshal(body, this)
	if err != nil {
		logrus.Error(err)

	}
	this.InitProxy(this)
	return this, err
}
func (this *IchubResult) DataIfResultParams() *IchubResultParams {

	var m = this.Data.(map[string]interface{})
	return NewIchubResultParams().ValueOf(m)
}

func (this *IchubResult) DataIfMap() map[string]interface{} {

	return this.Data.(map[string]interface{})
}
func (this *IchubResult) ValueByKey(key string) any {
	return this.Data.(map[string]any)[key]

}

func (this *IchubResult) CheckValueByKey(key string, expect any) bool {
	var value = this.ValueByKey(key)

	v, _ := strutil.String(value)
	exp, _ := strutil.String(expect)
	return v == exp
}

func (this *IchubResult) ToString() string {

	return jsonutils.ToJsonPretty(this)

}

func (this *IchubResult) Success() *IchubResult {
	this.Code = CODE_SUCCESS
	this.Msg = "成功"
	return this
}

func (this *IchubResult) SuccessData(data any) *IchubResult {
	this.Code = CODE_SUCCESS
	this.Msg = "成功"
	this.Data = data
	return this
}
func (this *IchubResult) SuccessMessage(msg string, data any) *IchubResult {
	this.Code = CODE_SUCCESS
	this.Msg = msg

	this.Data = data
	return this
}

func (this *IchubResult) Fail() *IchubResult {
	this.Code = CODE_FAIL
	this.Msg = "失败"
	return this
}

func (this *IchubResult) FailMessage(msg string) *IchubResult {
	this.Code = CODE_FAIL
	this.Msg = msg
	return this
}
func (this *IchubResult) CodeMsg(code int, msg string) *IchubResult {
	this.Code = code
	this.Msg = msg
	return this
}

func (this *IchubResult) SetData(s any) {
	this.Data = s
}

func (this *IchubResult) GetData() any {

	return this.Data
}

func (this *IchubResult) String() string {
	return jsonutils.ToJsonPretty(this)
}

func FailResult(msg string) *IchubResult {
	return &IchubResult{
		Code: CODE_FAIL,
		Msg:  msg,
	}
}

func (this *IchubResult) GetDbResult() map[string]any {
	if this.Data == nil {
		return map[string]any{}
	}
	var mapData = this.Data.(map[string]any)
	var dbResult = mapData["DbResult"].(map[string]any)
	return dbResult

}
func (this *IchubResult) GetEsResult() map[string]any {
	if this.Data == nil {
		return map[string]any{}
	}
	var mapData = this.Data.(map[string]any)
	var esResult = mapData["EsResult"].(map[string]any)
	return esResult

}
func (this *IchubResult) To(out interface{}) {
	var res = jsonutils.ToJsonPretty(this.Data)

	jsonutils.FromJson(res, &out)

}
