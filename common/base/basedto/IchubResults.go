package basedto

import (
	"encoding/json"
	"fmt"
	"gitee.com/ichub/godbquery/common/base/baseutils/jsonutils"
	"gitee.com/ichub/godbquery/common/ichublog"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
)

/*
	@Title    文件名称: ichubresults.go
	@Description  描述: 批量统一返回结构

	@Author  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
	@Update  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
*/

type IchubResults struct {
	BaseEntity
	Code int           `json:"code"`
	Msg  string        `json:"msg"`
	Data []IchubResult `json:"data,omitempty"`
}

// func DataOfIndex(1)
func (results *IchubResults) FromJson(body []byte) (*IchubResults, error) {

	var err = json.Unmarshal(body, results)
	if err != nil {
		fmt.Println(err)

	}
	return results, err
}

func ParseIchubResults(body []byte) *IchubResults {
	var result IchubResults
	jsonutils.FromJsonByte(body, &result)
	return &result
}

func (results *IchubResults) Log() {
	ichublog.IchubLog.Println("results=", jsonutils.ToJsonPretty(results))
	logrus.Println(results.ToString())
}

func (results *IchubResults) ValueOf(index int) *IchubResult {
	var value = results.Data[index]
	return &value
}

func (results *IchubResults) String() (s string) {
	s, _ = jsonutils.ToJson(results)

	return
}

func (results *IchubResults) ToString() string {
	s, _ := json.MarshalIndent(results, "", " ")
	return string(s)

}

func (results *IchubResults) Success() *IchubResults {
	results.Code = CODE_SUCCESS
	results.Msg = "成功"
	return results
}

func (results *IchubResults) SuccessData(data []IchubResult) *IchubResults {
	results.Code = CODE_SUCCESS
	results.Msg = "成功"
	results.Data = data
	return results
}
func (results *IchubResults) SuccessMessage(msg string, data []IchubResult) *IchubResults {
	results.Code = CODE_SUCCESS
	results.Msg = msg

	results.Data = data
	return results
}

func (results *IchubResults) Fail() *IchubResults {
	results.Code = CODE_FAIL
	results.Msg = "失败"
	return results
}

func (results *IchubResults) FailMessage(msg string) *IchubResults {
	results.Code = CODE_FAIL
	results.Msg = msg
	return results
}
func (results *IchubResults) CheckCode(suite suite.Suite, code int) {

	suite.Equal(code, results.Code)
}

func (results *IchubResults) SetData(s []IchubResult) {
	results.Data = s
}

func (results *IchubResults) GetData() []IchubResult {

	return results.Data
}
