package basedto

import "encoding/json"

type PageParam struct {
	Current  int `json:"current"`
	PageSize int `json:"page_size"`
	Total    int `json:"total"`
	Count    int `json:"count"`
}

func (pageParam *PageParam) String() string {
	s, _ := json.Marshal(pageParam)
	return string(s)

}

func (pageParam *PageParam) ToString() string {
	s, _ := json.MarshalIndent(pageParam, "", " ")
	return string(s)

}

type PageResult struct {
	JsonResult
	Page PageParam `json:"data"`
	Data []any     `json:"data"`
}

func (pageResult *PageResult) String() string {
	s, _ := json.Marshal(pageResult)
	return string(s)

}
func (pageResult *PageResult) ToString() string {
	s, _ := json.MarshalIndent(pageResult, "", " ")
	return string(s)

}
