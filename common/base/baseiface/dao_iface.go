package baseiface

/*
   @Title  文件名称 : Employeeiface.go
   @Description 描述: 服务Employeeiface

   @Author  作者: leijianming@163.com  时间(2024-03-26 12:46:24)
   @Update  作者: leijianming@163.com  时间(2024-03-26 12:46:24)

*/

import (
	"gitee.com/ichub/godbquery/common/base/pagedto"
)

type DaoIface interface {

	/*
	   @title     函数名称:  Query
	   @description      :  通用查询
	   @auth      作者   :  leijianming@163.com 时间: 2024-03-26 12:46:24
	   @param     输入参数名: param * dto.EmployeeQueryParam
	   @return    返回参数名: * dto.EmployeePageResult
	*/
	Query(param *pagedto.IchubPageRequest) *pagedto.IchubPageResult

	/*
	   @title     函数名称 :  Count
	   @description       :  通用查询计数
	   @auth      作者     : leijianming@163.com 时间: 2024-03-26 12:46:24
	   @param     输入参数名: param * dto.EmployeeQueryParam
	   @return    返回参数名: * dto.EmployeeJsonResult
	*/
	Count(queryParam *pagedto.IchubPageRequest) (int, error)

	/*
	   @title     函数名称: FindById(Id int32)
	   @description      : 根据主键查询记录
	   @auth      作者:     leijianming@163.com 时间: 2024-03-26 12:46:24
	   @param     输入参数名:Id int32
	   @return    返回参数名:*dto.EmployeeJsonResult
	*/
	FindById(Id int) (entity any, found bool, err error)

	/*
	   @title     函数名称: FindByIds(pks string)
	   @description      :
	              根据主键Id 查询多条记录  例子： FindByIds("1,36,39")
	   @auth      作者:     leijianming@163.com 时间: 2024-03-26 12:46:24
	   @param     输入参数名:Id int32
	   @return    返回参数名:*dto.EmployeeJsonResult
	*/
	FindByIds(pks string) (any, error)

	/*

	   @title     函数名称: DeleteById
	   @description      : 根据主键软删除。
	   @auth      作者   : leijianming@163.com 时间: 2024-03-26 12:46:24
	   @param     输入参数名: Id int32
	   @return    返回参数名: *basedto.JsonResult

	*/
	DeleteById(Id int) error

	/*

	   @title     函数名称: Save
	   @description      : 主键%s为nil,0新增; !=nil修改。
	   @auth      作者   : leijianming@163.com 时间: 2024-03-26 12:46:24
	   @param     输入参数名: entity *model.Employee
	   @return    返回参数名: *basedto.JsonResult
	*/
	//Save(entity any) (int,error)

	/*

	   @title     函数名称: UpdateNotNullProps
	   @description      : 更新非空字段
	   @auth      作者   : leijianming@163.com 时间: 2024-03-26 12:46:24
	   @param     输入参数名: Id int64,entity map[string]interface{}
	   @return    返回参数名: *basedto.JsonResult

	*/
	UpdateNotNull(Id int, entity map[string]interface{}) (int, error)
}
