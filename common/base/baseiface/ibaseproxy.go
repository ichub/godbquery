package baseiface

type IbaseProxy interface {
	String() string

	ToString() string
	Log()
	Clone() any

	FromJson(body []byte) any
	ToJson() string
	ValueOf(another any)
	//ValieFrom(a any)
}
