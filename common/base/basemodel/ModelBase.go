package basemodel

import "encoding/json"

type ModelBase struct {
	// gorm.Model
	// Id        *uint64     `gorm:"primary_key" json:"id"`
	CreatedAt *LocalDateInt `json:"created_at"`
	UpdatedAt *LocalDateInt `json:"updated_at"`
	DeletedAt *LocalDateInt `gorm:"column:deleted_at;type:timestamp" sql:"index" json:"-"`
	CreatedBy *uint64       `json:"create_by"`
	UpdatedBy *uint64       `json:"updated_by"`
	DeletedBy *uint64       `json:"deleted_by"`
}

func (entity *ModelBase) String() string {
	s, _ := json.Marshal(entity)
	return string(s)

}

func (entity *ModelBase) ToString() string {
	s, _ := json.MarshalIndent(entity, "", " ")
	return string(s)

}
