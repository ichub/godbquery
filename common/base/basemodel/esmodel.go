package basemodel

import (
	"context"
	"fmt"
	"github.com/olivere/elastic/v7"
	"github.com/sirupsen/logrus"
)

var esclient *elastic.Client
var eshost = "http://192.168.14.58:9200"

// 初始化
func EsInit() {
	var err error
	esclient, err = elastic.NewClient(elastic.SetSniff(false), elastic.SetURL(eshost))
	if err != nil {
		panic(err)
	}
	_, _, err = esclient.Ping(eshost).Do(context.Background())
	if err != nil {
		panic(err)
	}

	_, err = esclient.ElasticsearchVersion(eshost)
	if err != nil {
		panic(err)
	}
}

type EsModel struct {
	IndexName string
	TypeName  string
	ID        string
	Body      interface{}
}

func (es *EsModel) Save() {
	//e1 := Employee{"Jane", "Smith", 32, "I like to collect rock albums", []string{"music"}}
	put1, err := esclient.Index().
		Index(es.IndexName).Type(es.TypeName).Id(es.ID).
		BodyJson(es.Body).Do(context.Background())
	if err != nil {
		panic(err)
	}
	fmt.Printf("Indexed tweet %s to index s%s, type %s\n", put1.Id, put1.Index, put1.Type)

}

func (es *EsModel) update(m map[string]interface{}) {
	res, err := esclient.Update().Index(es.IndexName).Type(es.TypeName).Id(es.ID).
		Doc(m).Do(context.Background())
	if err != nil {
		println(err.Error())
	}
	fmt.Printf("update age %s\n", res.Result)

}

func (es *EsModel) DeleteById(id string) {

	res, err := esclient.Delete().Index(es.IndexName).Type(es.TypeName).Id(id).
		Do(context.Background())
	if err != nil {
		println(err.Error())
		return
	}
	fmt.Printf("DeleteById result %s\n", res.Result)
}

func (es *EsModel) FindById(id string) {
	//通过id查找
	getRes, err := esclient.Get().Index(es.IndexName).Type(es.TypeName).Id(id).Do(context.Background())
	if err != nil {
		panic(err.Error())
	}
	if getRes.Found {
		logrus.Printf("Got document %s in version %d from index %s, type %s\n", getRes.Id, getRes.Version, getRes.Index, getRes.Type)

		es.Body = []byte(getRes.Source)
	}

}
