package fileutils

import (
	"gitee.com/ichub/godbquery/common/base/baseconsts"
	"github.com/duke-git/lancet/fileutil"
	"github.com/duke-git/lancet/system"
	"github.com/sirupsen/logrus"
	"os"
	"strings"
)

func CheckConfigFileExist(winroot, f string) bool {
	return fileutil.IsExist(winroot + f)
}

func FindRootDir() string {

	return FindRootDirFor(GetCurPath(), baseconsts.DEFINE_ENV_PATHFILE)
}

func FindRootDirFor(curpath string, filename string) string {

	if system.IsLinux() {
		var rootdir = os.Getenv(baseconsts.IchubBasePath)
		if len(rootdir) == 0 {
			rootdir = curpath
		}
		os.Setenv(baseconsts.IchubBasePath, rootdir)
		return rootdir
	}

	var windir = FindRootDirWin(curpath, filename)
	os.Setenv(baseconsts.IchubBasePath, windir)
	return windir

}

func FindRootDirWin(wrPath, f string) string {

	var dirs = strings.Split(wrPath, "\\")
	if len(dirs) < 2 {
		dirs = strings.Split(wrPath, "/")
	}

	var workRoot = strings.Join(dirs, "\\")
	var i = len(dirs)
	for i > 0 {
		i--
		if CheckConfigFileExist(workRoot, f) {
			os.Setenv(baseconsts.IchubBasePath, workRoot)
			return workRoot
		}
		workRoot = strings.Join(dirs[0:i], "\\")
	}

	return workRoot
}

func GetCurPath() string {

	c, _ := os.Getwd()
	return c
}

func CheckFileExist(filename string) bool {

	if fileutil.IsExist(filename) {
		logrus.Debug("file  exist! ", filename)
		return true
	}

	logrus.Warnf("file not exists !%s\r\n", filename)
	return false

}

func UnZip(zipFile string, destPath string) error {
	return fileutil.UnZip(zipFile, destPath)
}

func Zip(srcPath string, destFile string) error {
	return fileutil.Zip(srcPath, destFile)
}
