package jsonutils

import (
	"encoding/json"
	jsoniter "github.com/json-iterator/go"
	"github.com/sirupsen/logrus"
)

type JsonUtils struct {
}

func ToJson(v any) (string, error) {
	result, err := jsoniter.MarshalIndent(v, "", "  ")
	if err != nil {
		logrus.Println(err)
		return "{}", err
	}
	return string(result), nil
}

func FromJson(str string, anyout any) error {

	err := jsoniter.UnmarshalFromString(str, anyout)
	if err != nil {
		logrus.Error(err)
		return err
	}
	return err
}
func FromJsonByte(str []byte, anyout any) error {

	err := jsoniter.Unmarshal(str, anyout)
	if err != nil {
		logrus.Error(err)
	}
	return err
}

func ToJsonStr(v any) string {
	result, err := json.Marshal(v)
	if err != nil {
		logrus.Error(err)
		return "{}"
	}
	return string(result)
}

func ToJsonPretty(v any) string {
	result, err := json.MarshalIndent(v, "", "     ")
	if err != nil {
		logrus.Error(err)
		return "{}"
	}
	return string(result)
}

func FromJsonStr(param []byte) any {
	var ret any
	_ = json.Unmarshal(param, &ret)
	return ret
}

func MapFromJson(param string) (m *map[string]any, err error) {
	m = &map[string]any{}
	err = FromJson(param, m)
	return m, err
}

func Str2JsonPretty(txt string) string {
	v := FromJsonStr([]byte(txt))
	return ToJsonPretty(v)
}

func Byte2JsonPretty(b []byte) string {
	v := FromJsonStr(b)
	return ToJsonPretty(v)
}
