package baseweb

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

type BaseWeb struct {
}

func (this *BaseWeb) Fclose(ctx *gin.Context) {
	if err := ctx.Request.Body.Close(); err != nil {
		logrus.Error("failed to close:", err)
	}
}
