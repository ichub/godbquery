package ichubmetadata

import (
	"encoding/json"
	"gitee.com/ichub/godbquery/common/base/basedto"
	"gitee.com/ichub/godbquery/common/base/metadata/indexmetadata"
	"strings"
)

/*
@Title    文件名称: metadata_table.go
@Description  描述: 元数据--表信息

@Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
*/
type MetadataTable struct {
	basedto.BaseEntity

	TableSchema  string            `json:"table_schema" gorm:"column:table_schema"`
	TableName    string            `json:"table_name" gorm:"column:table_name"`
	TableComment string            `json:"table_comment" gorm:"column:table_comment"`
	PkInfo       *MetadataPkInfo   `json:"pk_info,omitempty"`
	Columns      []MetadataColumn  `json:"columns"`
	GoFields     []MetadataGoField `json:"go_fields"`

	TableExist    bool                           `json:"table_exist"`
	FieldsName    string                         `json:"fields_name"`
	IndexName     string                         `json:"index_name"`
	IndexMetadata *indexmetadata.EsIndexMetadata `json:"index_metadata"`
}

func (this *MetadataTable) Parse2EsIndex() {
	this.IndexName = this.TableName
	this.IndexMetadata = indexmetadata.NewIndexMetadata()
	this.IndexMetadata.SetIndexName(this.IndexName)
	for _, c := range this.Columns {
		var estype = c.FindColEsType()
		this.IndexMetadata.AddField(c.ColumnName, estype)
	}
}
func (this *MetadataTable) ToMapping() map[string]any {
	this.Parse2EsIndex()
	return this.IndexMetadata.ToMapping()

}

func (this *MetadataTable) ToMappingStr() string {
	return this.IndexMetadata.ToMappingStr()

}
func NewMetadataTable() *MetadataTable {
	var meta = &MetadataTable{}
	meta.InitProxy(meta)
	meta.GoFields = []MetadataGoField{}
	meta.IndexMetadata = indexmetadata.NewIndexMetadata()
	return meta
}
func (this *MetadataTable) ToFieldsString() string {
	var sb = strings.Builder{}
	for _, v := range this.Columns {
		if sb.Len() == 0 {
			sb.WriteString(v.ColumnName)
		} else {
			sb.WriteString("," + v.ColumnName)
		}
	}
	return sb.String()

}

//build2GoFields

func (this *MetadataTable) BuildGoFields() {
	for _, v := range this.Columns {
		var gof = NewMetadatGoField()
		gof.DataType = v.DataType
		gof.ColumnName = v.ColumnName
		gof.GoType = v.FindColGoType() //(v.DataType)
		this.GoFields = append(this.GoFields, *gof)
	}
}

func (this *MetadataTable) ToString() string {
	s, _ := json.Marshal(this)
	return string(s)

}

func (this *MetadataTable) FindGoType(field string) string {
	for _, v := range this.GoFields {
		if v.ColumnName == field {
			return v.GoType
		}
	}
	return "string"
}
