package indexmetadata

import (
	"gitee.com/ichub/godbquery/common/base/baseutils/jsonutils"
	"github.com/sirupsen/logrus"
)

const mappings = "mappings"
const properties = "properties"

type Keyword struct {
	IgnoreAbove int    `json:"ignore_above"`
	Type        string `json:"type"`
}
type Fields struct {
	Keyword Keyword `json:"keyword,omitempty"`
}

func NewFields() *Fields {
	return &Fields{
		Keyword: Keyword{
			IgnoreAbove: 256,
			Type:        "keyword",
		},
	}
}

type MappingField struct {
	Fields *Fields `json:"fields,omitempty"`
	Type   string  `json:"type"`
}

func NewMappingField() *MappingField {
	return &MappingField{}
}

type PropertiesDto map[string]*MappingField

type EsIndexMetadata struct {
	IndexName  string        `json:"index_name"`
	Properties PropertiesDto `json:"properties"`
}

func NewIndexMetadata() *EsIndexMetadata {
	return &EsIndexMetadata{
		Properties: make(PropertiesDto),
	}
}
func (this *EsIndexMetadata) SetIndexName(index string) {
	this.IndexName = index
}

func (this *EsIndexMetadata) AddField(name, Type string) {
	this.Properties[name] = NewMappingField()
	this.Properties[name].Type = Type
	if Type == "text" {
		this.Properties[name].Fields = NewFields()
	}

}

// "ichub_sys_dept": {
// "mappings": {
// "properties": {

func (this *EsIndexMetadata) ToMapProp() map[string]any {

	var result = map[string]any{}
	result[properties] = this.Properties
	return result
}

func (this *EsIndexMetadata) ToMapping() map[string]any {

	var result = map[string]any{}
	result[mappings] = this.ToMapProp()
	return result
}

func (this *EsIndexMetadata) ToMappings() map[string]any {

	var result = map[string]any{}
	result[this.IndexName] = this.ToMapping()
	logrus.Info(jsonutils.ToJsonPretty(result))
	return result
}

func (this *EsIndexMetadata) ToMappingStr() string {

	var s = jsonutils.ToJsonPretty(this.ToMapping())
	logrus.Info(s)
	return s
}
