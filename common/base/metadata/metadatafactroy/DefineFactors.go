package metadatafactroy

import (
	"container/list"
	"gitee.com/ichub/godbquery/common/base/metadata/ichubmetadata"
	"gitee.com/ichub/godbquery/common/ichubconfig"
)

var Cfg = ichubconfig.NewConfig()

type DefineFactors struct {
	Columns *[]ichubmetadata.MetadataColumn
	Models  *list.List
	//mappings
}
