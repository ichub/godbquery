package metadatafactroy

import "gitee.com/ichub/godbquery/common/base/metadata/ichubmetadata"

type ImetadataFactroy interface {
	FindMetadata(table string) *ichubmetadata.MetadataTable
	FindFields(table string, fields string) string
}
