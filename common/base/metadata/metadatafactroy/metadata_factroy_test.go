package metadatafactroy

import (
	"github.com/sirupsen/logrus"
	"os"
	"testing"
)

func init() {
	os.Chdir("../../../../")
}

var DbFactry = NewMetadataFactroy()

func Test0001_FindMetadata(t *testing.T) {

	var table = DbFactry.FindMetadata("rules")
	logrus.Info(table, table.ToFieldsString())
}
func Test0002_FindMetadataIndex(t *testing.T) {

	var table = DbFactry.FindMetadata("rules")
	logrus.Info(table, table.ToMappingStr())
}
