package pagedto

import (
	"gitee.com/ichub/godbquery/common/base/basedto"
	"gitee.com/ichub/godbquery/common/base/baseutils/jsonutils"
	"gitee.com/ichub/godbquery/common/ichubcontext"
	"gitee.com/ichub/godbquery/common/ichublog"
)

/*
@Title    文件名称: IchubPageResult.go
@Description  描述:  IchubPageResult

@Author  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
*/
type IchubPageResult struct {
	basedto.BaseEntity
	Code int    `json:"code"`
	Msg  string `json:"msg"`

	PageSize    int `json:"page_size"`
	PageCurrent int `json:"current"`

	Total int         `json:"total"`
	Data  interface{} `json:"data"`

	// Icode , Imsg string
}

func NewIchubPageResult() *IchubPageResult {
	var resp = &IchubPageResult{
		Code: 200,
		Msg:  "成功",
	}

	resp.InitProxy(resp)
	return resp

}

func NewPageResult(code int, msg string) *IchubPageResult {
	var resp = &IchubPageResult{
		Code: code,
		Msg:  "成功",
	}

	resp.InitProxy(resp)
	return resp
}
func NewPageResultError(msg string) *IchubPageResult {
	var resp = &IchubPageResult{
		Code: 500,
		Msg:  msg,
	}

	resp.InitProxy(resp)
	return resp
}
func PageResultOf(req *IchubPageRequest) *IchubPageResult {
	var this = NewIchubPageResult()
	this.PageSize = req.PageSize
	this.PageCurrent = req.PageCurrent
	return this
}
func (this *IchubPageResult) CodeMsg(Code int, Msg string) *IchubPageResult {
	this.Code = Code
	this.Msg = Msg
	return this
}
func (this *IchubPageResult) Success() *IchubPageResult {
	this.CodeMsg(200, "成功")
	return this
}
func (this *IchubPageResult) OkMsg(Msg string) *IchubPageResult {
	this.CodeMsg(200, Msg)
	return this
}
func (this *IchubPageResult) FailMsg(Msg string) *IchubPageResult {

	this.CodeMsg(500, Msg)
	return this
}

func (this *IchubPageResult) To(out interface{}) {
	var res = jsonutils.ToJsonPretty(this.Data)

	jsonutils.FromJson(res, out)

}
func (this *IchubPageResult) WriteDaoFile(table string) {
	var fileName = table + "_pageresult.json"
	ichubcontext.CommonContext.WriteDaoFile(fileName, this.ToPrettyString())
	ichublog.Log(this.ToPrettyString())

}
