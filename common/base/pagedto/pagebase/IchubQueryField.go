package pagebase

import (
	"gitee.com/ichub/godbquery/common/base/basedto"
	"gitee.com/ichub/godbquery/common/base/baseutils"
)

/*
@Title    文件名称: IchubQueryField.go
@Description  描述:  IchubQueryField

@Author  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
*/

// 通用查询条件
type IchubQueryField struct {
	basedto.BaseEntity
	// 字段名 小写
	Field string `json:"field"`
	//比较符 = != > < >= <= between notbetween in notin like notlike
	OpType FieldSign `json:"op_type"`
	//比较值：一个或多个值
	Values []interface{} `json:"values,omitempty"`
}

func NewQueryFields() *IchubQueryField {
	var bfields = &IchubQueryField{}
	bfields.InitProxy(bfields)
	return bfields
}

func NewFields(field string, opType int, opValues []any) *IchubQueryField {

	var queryFields = &IchubQueryField{
		Field:  field,
		OpType: OpSign[opType],
		Values: opValues,
	}
	queryFields.InitProxy(queryFields)
	return queryFields
}
func (this *IchubQueryField) CheckType() string {
	return baseutils.CheckType(this.Values[0])
}

func (this *IchubQueryField) SetField(field string) {
	this.Field = field
}

func (this *IchubQueryField) Values2InStr() string {
	var s = ""
	for _, v := range this.Values {
		if len(s) > 0 {
			s += ","
		}
		if this.CheckType() == "string" {
			s += `"` + baseutils.Any2Str(v) + `"`
		} else {
			s += baseutils.Any2Str(v)
		}

	}
	return s

}
func (this *IchubQueryField) Field2Keyword() string {
	return this.Field + ".keyword"
}
