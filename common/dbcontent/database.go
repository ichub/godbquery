package dbcontent

import (
	"gitee.com/ichub/godbquery/common/base/baseconsts"
	"gitee.com/ichub/godbquery/common/dbcontent/database"
	"gitee.com/ichub/godbquery/common/ichubconfig"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/sirupsen/logrus"
	"sync"
)

/*
	@Title    文件名称: database.go
	@Description  描述: dbclient客户端

	@Author  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
	@Update  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
*/

var DbClient = database.NewIchubDbClient()
var rw sync.RWMutex
var IchubConig = ichubconfig.NewConfig()

func GetDB() *gorm.DB {

	if DbClient.Db == nil {
		rw.Lock()
		defer rw.Unlock()
		Ini()
	}
	return DbClient.DbDebug()
}

func Ini() *gorm.DB {

	var ds = IchubConig.ReadIchubDb()
	DbClient.ValueOf(ds)
	var err = DbClient.IniDb()
	if err != nil {
		logrus.Fatal(err)
	}
	if IchubConig.Gorm.Debug == "true" {
		return DbClient.DbDebug()
	}
	return DbClient.Db

}
func IniDbType(dbtype string) *gorm.DB {

	var ds = IchubConig.ReadIchubDatasource()
	if dbtype == baseconsts.DB_TYPE_MYSQL {
		ds = IchubConig.ReadIchubMysql()
	}
	DbClient.ValueOf(ds)
	var err = DbClient.IniDb()
	if err != nil {
		logrus.Error(err)
	}
	return DbClient.DbDebug()

}
