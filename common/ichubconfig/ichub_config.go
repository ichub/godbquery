package ichubconfig

import (
	"gitee.com/ichub/godbquery/common/base/baseconfig"
	"gitee.com/ichub/godbquery/common/base/baseconsts"
	"gitee.com/ichub/godbquery/common/base/baseutils/fileutils"
	"gitee.com/ichub/godbquery/common/ichublog"
	"github.com/duke-git/lancet/system"
	"github.com/jinzhu/copier"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"os"
	"strings"
)

/*
	@Title    文件名称: ichub_config.go
	@Description  描述:  配置文件

	@Author  作者: leijianming@163.com  时间(2024-02-22 22:38:21)
	@Update  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
*/

const ConfigfileIchubCommon = "/config/ichubcommon.yml"
const ConfigfileIchubengine = "/config/app.yml"
const ConfigfileIchubfactroy = "/config/factroy/ichubfactroy.yml"
const ConfigfileIchubdemo = "/config/ichubengine-demo.yml"

type IchubConfig struct {
	baseconfig.IchubClientDto

	Env      string `json:"env"`
	BasePath string `json:"base_path"`

	EnvConfigFile  string `json:"env_config_file"`
	ConfigFile     string `json:"config_file"`
	RealConfigFile string `json:"real_config_file"`

	Etcd struct {
		Server struct {
			Hosturl string `json:"hosturl"`
		}
	}

	Rpc struct {
		ServerName string `json:"server_name"`
		ServerPort int    `json:"server_port"`
		ClientName string `json:"client_name"`
	}

	Web struct {
		Server struct {
			Name string `json:"name"`
			Port int    `json:"port"`
		}
		Client struct {
			Name    string `json:"name"`
			TestUrl string `json:"test_url"`
		}
		Swagger struct {
			Host     string `json:"host"`
			BasePath string `json:"base_path"`
			Version  string `json:"version"`
			Title    string `json:"title"`
			Enable   string `json:"enable"`
		}
	}

	Datasource struct {
		Dbtype string `json:"dbtype"`
		Dbname string `json:"dbname"`
		Host   string `json:"host"`
		Port   string `json:"port"`

		Username string `json:"username"`
		Password string `json:"password"`
		Sslmode  string `json:"sslmode"`
		Charset  string `json:"charset"`
	}

	Redis struct {
		Db       int    `json:"db"`
		Addr     string `json:"addr"`
		Password string `json:"password"`
	} `json:"redis"`

	Nats struct {
		Url string `json:"url"`
	}
	Es struct {
		Url      string `json:"url"`
		Username string `json:"username,omitempty"`
		Password string `json:"password,omitempty"`
	}
	Mysql struct {
		Dbtype string `json:"dbtype"`
		Dbname string `json:"dbname"`
		Host   string `json:"host"`
		Port   string `json:"port"`

		Username string `json:"username"`
		Password string `json:"password"`
		Sslmode  string `json:"sslmode"`
		Charset  string `json:"charset"`
	}
	Gorm    baseconfig.GormClientDto
	Factroy baseconfig.FactroyClientDto
}

func New(configFile string) *IchubConfig {
	var ichubConfig = &IchubConfig{
		ConfigFile: configFile,
	}
	ichubConfig.InitProxy(ichubConfig)
	return ichubConfig
}
func NewConfig() *IchubConfig {
	return New(ConfigfileIchubengine)
}

func NewCommon() *IchubConfig {

	return New(ConfigfileIchubCommon)
}

func (this *IchubConfig) Log() {
	ichublog.Log(this)
}

func (this *IchubConfig) FullFileName() string {
	if len(this.BasePath) == 0 {
		this.BasePath = os.Getenv(baseconsts.IchubBasePath)
	}
	return this.BasePath + this.ParseRealFileName()
}

func (this *IchubConfig) ParseRealFileName() string {

	var files = strings.Split(this.ConfigFile, ".")
	var env = ""
	if len(this.Env) > 0 {
		env = "-" + this.Env
	}
	this.RealConfigFile = files[0] + env + "." + files[1]
	return this.RealConfigFile
}

func (this *IchubConfig) EnvFileName() string {
	this.EnvConfigFile = baseconsts.ConfigfileIchubEnv
	if len(this.BasePath) == 0 {
		this.BasePath = os.Getenv(baseconsts.IchubBasePath)
	}
	return this.BasePath + this.EnvConfigFile
}

func (this *IchubConfig) LogFileName() {
	logrus.Info(this.FullFileName())
}

func (this *IchubConfig) CheckFileExist() bool {

	return fileutils.CheckFileExist(this.FullFileName())

}
func (this *IchubConfig) CheckEnvFileExist() bool {

	return fileutils.CheckFileExist(this.EnvFileName())

}
func (this *IchubConfig) SetDefaultBasePath(basepath string) {
	if len(basepath) == 0 {
		basepath = system.GetOsEnv(baseconsts.IchubBasePath)
	}
	if len(basepath) == 0 {
		basepath = fileutils.GetCurPath()
	}

	if this.ConfigFile == "" {
		this.ConfigFile = ConfigfileIchubengine
	}
	this.BasePath = basepath
}
func (this *IchubConfig) ReadConfigEnv() string {

	if !this.CheckEnvFileExist() {
		logrus.Warn("file not exist!" + this.EnvFileName())
	}

	viper := viper.New()
	viper.SetConfigType("yaml")
	viper.SetConfigFile(this.EnvFileName())

	err := viper.ReadInConfig()
	if err != nil {
		logrus.Error(err)
		return ""
	}
	err = viper.Unmarshal(this)
	if err != nil {
		logrus.Error(err)
		return ""
	}
	logrus.Info("ReadConfigEnv=", this.Env)

	this.Env = this.ParseValue("env", this.Env)
	if this.Env == baseconsts.ENV_DEFAULT {
		this.Env = baseconsts.ENV_EMPTY
	}

	return this.Env

}
func (this *IchubConfig) ReadConfig(basePath string) {
	this.SetDefaultBasePath(basePath)
	this.ReadConfigEnv()
	if !this.CheckFileExist() {
		panic("file not exist!" + this.FullFileName())
	}

	ichublog.Log(this.FullFileName())
	viper := viper.New()
	viper.SetConfigType("yaml")
	viper.SetConfigFile(this.FullFileName())

	err := viper.ReadInConfig()
	if err != nil {
		logrus.Error(err)
		return
	}
	err = viper.Unmarshal(this)
	if err != nil {
		logrus.Error(err)
	}
	this.IchubClientDto.Parse()
	logrus.Info("ReadConfig=", this)

}

func (this *IchubConfig) SetOsEnv(key, value string) *IchubConfig {
	system.SetOsEnv(key, value)
	return this
}

func (this *IchubConfig) Read() *IchubConfig {
	var basepath = system.GetOsEnv(baseconsts.IchubBasePath)
	if len(basepath) == 0 {
		basepath = fileutils.GetCurPath()
	}
	this.ReadConfig(basepath)
	this.Gorm.DbType = this.ParseValue("dbtype", this.Gorm.DbType)
	ichublog.Log(this.ToPrettyString())

	return this

}
func (this *IchubConfig) ReadIchubDb() *baseconfig.DbClientDto {

	this.Read()
	if this.Gorm.DbType == baseconsts.DB_TYPE_MYSQL {
		return this.ReadIchubMysql()
	} else if this.Gorm.DbType == baseconsts.DB_TYPE_COCKROACH {
		return this.ReadIchubDatasource()
	}
	return this.ReadIchubDatasource()
}

func (this *IchubConfig) ReadIchubDatasource() *baseconfig.DbClientDto {

	this.Read()
	var dbClientDto = baseconfig.NewDbClientDto()
	copier.Copy(dbClientDto, &this.Datasource)

	dbClientDto.GormClient = this.ReadIchubGorm()
	return dbClientDto
}

func (this *IchubConfig) ReadIchubMysql() *baseconfig.DbClientDto {

	this.Read()
	var dbClientDto = baseconfig.NewDbClientDto()
	copier.Copy(dbClientDto, &this.Mysql)
	dbClientDto.GormClient = this.ReadIchubGorm()

	return dbClientDto
}

func (this *IchubConfig) ReadIchubGorm() *baseconfig.GormClientDto {

	this.Read()
	var dto = baseconfig.NewGormClientDto()
	dto.CopyWithOption(this.Gorm)
	return dto
}

func (this *IchubConfig) ReadIchubFactroy() baseconfig.FactroyClientDto {

	this.Read()
	return this.Factroy
}

func (this *IchubConfig) ReadIchubRpc() *baseconfig.RpcServerDto {

	this.Read()
	var dto = &baseconfig.RpcServerDto{
		EtcdHost:   this.Etcd.Server.Hosturl,
		ServerName: this.Rpc.ServerName,
		ServerPort: this.Rpc.ServerPort,
		ClientName: this.Rpc.ClientName,
	}
	return dto
}

func (this *IchubConfig) ReadIchubEs() *baseconfig.ElasticClientDto {

	this.Read()
	var dto = baseconfig.NewElasticClientDto()
	dto.URL = this.Es.Url
	dto.Username = this.Es.Username
	dto.Password = this.Es.Password

	return dto
}
func (this *IchubConfig) ReadIchubRedis() *baseconfig.RedisClientDto {

	this.Read()
	var dto = &baseconfig.RedisClientDto{
		Addr:     this.Redis.Addr,
		Password: this.Redis.Password,
		DB:       this.Redis.Db,
	}
	dto.InitProxy(dto)
	return dto
}
func (this *IchubConfig) ReadIchubWebServer() (serverDto *baseconfig.WebServerDto) {

	this.Read()
	serverDto = baseconfig.NewWebServerDto()

	serverDto.ServerName = this.Web.Server.Name
	serverDto.ServerPort = this.Web.Server.Port
	serverDto.EtcdHost = this.Etcd.Server.Hosturl
	return serverDto
}

func (this *IchubConfig) ReadIchubWebClient() (clientDto *baseconfig.WebClientDto) {

	this.Read()
	clientDto = &baseconfig.WebClientDto{}
	clientDto.ServerName = this.Web.Client.Name
	clientDto.TestUrl = this.Web.Client.TestUrl
	clientDto.EtcdHost = this.Etcd.Server.Hosturl
	return clientDto

}

func (this *IchubConfig) ReadIchubCommon() *IchubConfig {

	this.ConfigFile = ConfigfileIchubCommon
	this.Read()
	return this
}

func (this *IchubConfig) ReadWebSwagger() *baseconfig.SwaggerClientDto {

	this.Read()
	var swagger = baseconfig.NewSwaggerClientDto()

	swagger.Host = this.Web.Swagger.Host
	swagger.BasePath = this.Web.Swagger.BasePath
	swagger.Version = this.Web.Swagger.Version
	swagger.Title = this.Web.Swagger.Title
	swagger.Enable = this.Web.Swagger.Enable

	return swagger
}
