package ichubconfig

import (
	"fmt"
	"gitee.com/ichub/godbquery/common/base/basedto"
	"gitee.com/ichub/godbquery/common/base/baseutils"
	"gitee.com/ichub/godbquery/common/base/baseutils/fileutils"
	"github.com/go-ini/ini"
	"github.com/sirupsen/logrus"
)

const general = "general"
const cms = "cms"

type IchubConfigIni struct {
	basedto.BaseEntity
	ConfigFileName string
	funcDefineMap  map[string]string

	cfg *ini.File `json:"-"`
}

func NewIchubConfigIni() *IchubConfigIni {
	var ini = &IchubConfigIni{}
	ini.InitProxy(ini)
	return ini
}

func (this *IchubConfigIni) Find2Map() map[string]string {

	var maps = this.FindMap(general)
	var mapcms = this.FindMap(cms)
	for k, v := range mapcms {
		maps["cms:"+k] = v
	}
	logrus.Debug(maps)
	return maps

}

func (this *IchubConfigIni) FindMap(domain string) map[string]string {
	var sec = this.Find(domain)
	var funcmap = map[string]string{}

	for _, v := range sec.Keys() {
		funcmap[v.Name()] = v.Value()
	}
	this.funcDefineMap = funcmap
	if len(sec.Keys()) != len(funcmap) {
		logrus.Error("func define有重复！")
	}
	return funcmap
}

// logrus.Debug(cfgIni.Find("general"))
func (this *IchubConfigIni) FindGeneral() *ini.Section {
	return this.Find(general)
}
func (this *IchubConfigIni) FindCms() *ini.Section {
	return this.Find(cms)
}
func (this *IchubConfigIni) Find(key string) *ini.Section {
	return this.cfg.Section(key)

	//	Username:     cfg.Section("mysql").RuleKey("user").String(),
}
func (this *IchubConfigIni) Load(defFile string) error {
	// defFile = ichubcontext.CommonContext.DefineRuleFile
	this.ConfigFileName = defFile
	if !this.CheckFileExist(defFile) {
		return basedto.NewIchubError(500, "文件不存在！"+defFile)
	}
	cfg, err := baseutils.LoadIniCfg(defFile)

	if err != nil {
		fmt.Println(err)
		return err
	}
	fmt.Println(cfg)

	this.cfg = cfg
	return nil
}
func (this *IchubConfigIni) CheckFileExist(filename string) bool {

	return fileutils.CheckFileExist(filename)

}
