package ichubconfig

import (
	"gitee.com/ichub/godbquery/common/base/baseconsts"
	"gitee.com/ichub/godbquery/common/base/baseutils/fileutils"
	"gitee.com/ichub/godbquery/common/ichublog"
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

func init() {
	ichublog.InitLogrus()
	fileutils.FindRootDir()

}
func Test001_LogString(t *testing.T) {
	var ichubConfig = NewConfig()
	ichubConfig.Read()
	ichublog.Log(os.Getenv(baseconsts.IchubBasePath))
}

func Test002_ReadIchubGorm(t *testing.T) {
	var ichubConfig = NewConfig()
	var dto = ichubConfig.ReadIchubGorm()

	ichublog.Log(ichubConfig, dto)
}
func Test003_TestParseEnvVar(t *testing.T) {
	os.Setenv("WEB_SERVER_HOST", "huawei.web.test")
	var ichubConfig = NewConfig().Read()

	assert.Equal(t, os.Getenv("WEB_SERVER_HOST"), ichubConfig.Web.Server.Name)
	ichublog.Log(ichubConfig)
}
func Test004_TestReadFactroy(t *testing.T) {
	os.Setenv("WEB_SERVER_HOST", "huawei.web.test")
	var ichubConfig = NewConfig().ReadIchubFactroy()

	ichublog.Log(ichubConfig)
}
