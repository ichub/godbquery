package ichubcontext

import (
	"gitee.com/ichub/godbquery/common/base/baseconfig"
	"gitee.com/ichub/godbquery/common/base/baseconsts"
	"gitee.com/ichub/godbquery/common/dbcontent/database"
	"gitee.com/ichub/godbquery/common/ichubconfig"
	"gitee.com/ichub/godbquery/common/ichubelastic"
	"gitee.com/ichub/godbquery/common/ichubredis"
	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
)

var IchubClient = NewIchubClientService()

type IchubClientFactroy struct {
	ichubConfig *ichubconfig.IchubConfig

	dbDto    *baseconfig.DbClientDto
	dbClient *database.IchubDbClient

	esDto    *baseconfig.ElasticClientDto
	esclient *ichubelastic.ElasticClient

	redisDto    *baseconfig.RedisClientDto
	redisClient *ichubredis.IchubRedisClient

	mysqlDto    *baseconfig.DbClientDto
	mysqlClient *database.IchubDbClient
	dsDto       *baseconfig.DbClientDto
	dssqlClient *database.IchubDbClient
}

func (this *IchubClientFactroy) DbClient() *database.IchubDbClient {
	return this.dbClient
}

// v	suite.dbinst = ichubcontext.IchubClient.IniDbClient().DbClient().Db
func (this *IchubClientFactroy) GetDB() *gorm.DB {
	return this.IniDbClient().dbClient.DbDebug()
}

func (this *IchubClientFactroy) SetDbClient(dbClient *database.IchubDbClient) {
	this.dbClient = dbClient
}

func (this *IchubClientFactroy) RedisClient() *ichubredis.IchubRedisClient {
	return this.redisClient
}

func (this *IchubClientFactroy) SetRedisClient(redisClient *ichubredis.IchubRedisClient) {
	this.redisClient = redisClient
}

func (this *IchubClientFactroy) Esclient() *ichubelastic.ElasticClient {
	return this.esclient
}

func (this *IchubClientFactroy) SetEsclient(esclient *ichubelastic.ElasticClient) {
	this.esclient = esclient
}

func NewIchubClientService() *IchubClientFactroy {
	var service = &IchubClientFactroy{
		ichubConfig: ichubconfig.NewConfig(),
	}

	return service
}
func (this *IchubClientFactroy) IniRedisClient() *IchubClientFactroy {
	if this.redisClient == nil {
		this.ichubConfig.Read()
		this.redisDto = this.ichubConfig.ReadIchubRedis()
		this.redisClient = ichubredis.NewIchubRedis()
		this.redisClient.SetRedisClientDto(this.redisDto)
		this.redisClient.Open()
	}
	return this
}
func (this *IchubClientFactroy) IniEsClient() *IchubClientFactroy {
	if this.esDto == nil {
		this.ichubConfig.Read()
		this.esDto = this.ichubConfig.ReadIchubEs()
		this.esclient = ichubelastic.New(this.esDto)
		this.esclient.Open()
	}
	return this
}

func (this *IchubClientFactroy) IniDbClient() *IchubClientFactroy {
	if this.dbDto == nil {
		this.ichubConfig.Read()
		this.dbDto = this.ichubConfig.ReadIchubDb()
		this.dbClient = database.NewIchubDbClient()
		this.dbClient.Valueof(this.dbDto)
		this.dbClient.IniDb()
	}
	return this
}

func (this *IchubClientFactroy) Ini() *IchubClientFactroy {
	this.ichubConfig.Read()
	this.IniRedisClient()
	this.IniDbClient()
	return this.IniEsClient()
}
func (this *IchubClientFactroy) IniMysqlClient() *IchubClientFactroy {
	if this.dbDto == nil {
		this.ichubConfig.Read()
		this.mysqlDto = this.ichubConfig.ReadIchubMysql()

		this.mysqlClient = database.NewIchubDbClient()
		this.mysqlClient.Valueof(this.mysqlDto)
		err := this.mysqlClient.IniDb()
		if err != nil {
			panic(err)
		}
	}
	return this
}
func (this *IchubClientFactroy) IniDsClient() *IchubClientFactroy {
	if this.dbDto == nil {
		this.ichubConfig.Read()
		this.dsDto = this.ichubConfig.ReadIchubDatasource()
		this.dssqlClient = database.NewIchubDbClient()
		this.dssqlClient.Valueof(this.dsDto)
		var err = this.dssqlClient.IniDb()
		if err != nil {
			panic(err)
		}
	}
	return this
}
func (this *IchubClientFactroy) GetIchubDbClient(dbtype string) *database.IchubDbClient {
	if dbtype == baseconsts.DB_TYPE_MYSQL {
		this.IniMysqlClient()
		return this.mysqlClient
	}
	if dbtype == "cockroach" {
		this.IniDsClient()
		logrus.Info(this.dsDto)
		return this.dssqlClient
	}

	this.IniDbClient()
	return this.dbClient
}

func (this *IchubClientFactroy) GetDbClientDto(dbtype string) *baseconfig.DbClientDto {
	if dbtype == baseconsts.DB_TYPE_MYSQL {
		return this.mysqlDto
	}
	if dbtype == "cockroach" {

		return this.dsDto
	}
	return this.dbDto
}
