package ichubcontext

import (
	"gitee.com/ichub/godbquery/common/base/baseconsts"
	"gitee.com/ichub/godbquery/common/base/basedto"
	"gitee.com/ichub/godbquery/common/base/baseutils/fileutils"
	"gitee.com/ichub/godbquery/common/ichubconfig"
	"gitee.com/ichub/godbquery/common/ichublog"
	"github.com/duke-git/lancet/fileutil"
	"github.com/sirupsen/logrus"
	"go.uber.org/dig"
)

/*
	@Title    文件名称: ichub_context.go
	@Description  描述:  全局上下文

	@Author  作者: leijianming@163.com  时间(2024-02-22 22:38:21)
	@Update  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
*/

var CommonContext *IchubCommonContext = newIchubCommonContext()

const (
	DEFINE_RULE_PATH = "/config/rule_define/"
	DEFILE_RULE_FILE = "rule_define.ini"
	DAO_RULE_PATH    = "/data/output/dao/"

	//used by data-file
	INPUT_RULE_PATH  = "/data/input/rule/"
	OUTPUT_RULE_PATH = "/data/output/rule/"
)

type IchubCommonContext struct {
	basedto.BaseEntity
	Container *dig.Container `json:"-"`

	BasePath       string `json:"base_path"`
	DefineRulePath string `json:"define_rule_path"`
	DefineRuleFile string `json:"define_rule_file"`

	outRulePath string `json:"out_rule_path"`
	inRulePath  string `json:"in_rule_path"`
	DaoFilePath string `json:"dao_file_path"`

	IchubConfigIni *ichubconfig.IchubConfigIni `json:"="`
	FuncDefineMap  map[string]string           `json:"func_define_map"`

	ApiAll map[string]any `json:"-"`
}

func newIchubCommonContext() *IchubCommonContext {
	var ctxt = &IchubCommonContext{}
	ctxt.init()
	ctxt.InitProxy(ctxt)
	ichublog.InitLogrus()
	return ctxt
}

func (this *IchubCommonContext) init() {

	this.BasePath = fileutils.FindRootDir()

	this.DefineRulePath = this.BasePath + DEFINE_RULE_PATH
	this.DefineRuleFile = this.DefineRulePath + DEFILE_RULE_FILE
	this.inRulePath = this.BasePath + INPUT_RULE_PATH
	this.outRulePath = this.BasePath + OUTPUT_RULE_PATH
	this.DaoFilePath = this.BasePath + DAO_RULE_PATH

	this.LoadRuleDefine()
}

func (this *IchubCommonContext) OutRulePath() string {
	return this.outRulePath
}

func (this *IchubCommonContext) SetOutRulePath(outRulePath string) {
	this.outRulePath = outRulePath
}

func (this *IchubCommonContext) InRulePath() string {
	return this.inRulePath
}

func (this *IchubCommonContext) SetInRulePath(inRulePath string) {
	this.inRulePath = inRulePath
}

func (this *IchubCommonContext) NewContainer() *dig.Container {
	this.Container = dig.New()
	return this.Container
}

func (this *IchubCommonContext) CheckConfigFileExist() bool {
	return fileutils.CheckFileExist(this.BasePath + baseconsts.DEFINE_ENV_PATHFILE)
}

func (this *IchubCommonContext) LoadRuleDefine() error {
	this.IchubConfigIni = ichubconfig.NewIchubConfigIni()
	err := this.IchubConfigIni.Load(this.DefineRuleFile)
	if err != nil {
		logrus.Error(err.Error())
		return err
	}

	this.FuncDefineMap = this.IchubConfigIni.Find2Map()
	return err
}

func (this *IchubCommonContext) WriteDaoFile(daofile string, content string) error {
	fileutil.RemoveFile(this.DaoFilePath + daofile)
	err := fileutil.WriteStringToFile(this.DaoFilePath+daofile, content, false)
	if err != nil {
		logrus.Error(err)
	}
	return err
}
