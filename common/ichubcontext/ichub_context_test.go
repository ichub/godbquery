package ichubcontext

import (
	"fmt"
	"gitee.com/ichub/godbquery/common/base/baseutils/jsonutils"
	"gitee.com/ichub/godbquery/common/ichubconfig"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

var cfgIni = ichubconfig.NewIchubConfigIni() // NewIchubConfigIni()

func Test0001_cfgIni(t *testing.T) {

	err := CommonContext.LoadRuleDefine()
	if err != nil {
		panic(err.Error())
	}
	cfgIni = CommonContext.IchubConfigIni
	var keys = cfgIni.FindGeneral().Keys()
	logrus.Debug(jsonutils.ToJsonPretty(keys))
	cfgIni.Log()
	for _, v := range keys {
		logrus.Println(v)
	}
	logrus.Debug(cfgIni.Find2Map())

}

var config = ichubconfig.NewConfig()

func Test0002_readSwaagerVarNo(t *testing.T) {

	var client = config.ReadWebSwagger()
	fmt.Println(client.ToPrettyString())

}
func Test0003_readSwaagerVarNo(t *testing.T) {
	os.Setenv("SWAGGER_HOST", "192.168.14.58:88")

	var client = config.ReadWebSwagger()
	fmt.Println(client.ToPrettyString())
	assert.Equal(t, "192.168.14.58:88", client.Host)

}
func Test0004_cfgIni(t *testing.T) {

	err := cfgIni.Load(CommonContext.DefineRuleFile)
	if err != nil {
		panic(err.Error())
	}
	var keys = cfgIni.FindGeneral().Keys()
	fmt.Println(jsonutils.ToJsonPretty(keys))
	cfgIni.Log()
	for _, v := range keys {
		logrus.Println(v)
	}
	cfgIni.Find2Map()
}
func Test0005_cfgIni(t *testing.T) {

	var config = ichubconfig.NewConfig()

	config.Read()

	logrus.Info(config)
}
