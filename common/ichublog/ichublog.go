package ichublog

import (
	"gitee.com/ichub/godbquery/common/base/baseconsts"
	"gitee.com/ichub/godbquery/common/base/baseutils"
	"github.com/sirupsen/logrus"
)

var IchubLog = baseutils.NewIchubLogger(baseconsts.ICHUBENGINGE_LOG_FILE)

var IchubLogFactroy = baseutils.NewIchubLogger(baseconsts.ICHUBFACTROY_LOG_FILE)
var IchubLogWeb = baseutils.NewIchubLogger(baseconsts.ICHUBWEB_LOG_FILE)

func Log(ps ...any) {
	IchubLog.Println(ps)

	logrus.Info(ps)

}
func Error(ps ...any) {
	IchubLog.Println(ps)

	logrus.Error(ps)

}

func init() {
	InitLogrus()
}
func InitLogrus() {
	logrus.SetLevel(logrus.DebugLevel)
	logrus.SetReportCaller(true)

	logrus.SetFormatter(&logrus.TextFormatter{
		ForceColors:               true,
		EnvironmentOverrideColors: true,
		TimestampFormat:           "2006-01-12 15:04:05", //时间格式
		FullTimestamp:             true,
		DisableLevelTruncation:    true,
	})

}
