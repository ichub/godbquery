package ichubredis

import (
	"gitee.com/ichub/godbquery/common/base/baseconfig"
	"gitee.com/ichub/godbquery/common/base/baseutils/jsonutils"
	"github.com/duke-git/lancet/convertor"
	"github.com/go-redis/redis"
	"time"
)

var RedisInst *IchubRedisClient

const ExpireTime = 1000000 * 10
const PREKEY_USER = "user::"

type IchubRedisClient struct {
	Client         *redis.Client `json:"-"`
	expire         time.Duration
	redisClientDto *baseconfig.RedisClientDto
}

func (this *IchubRedisClient) RedisClientDto() *baseconfig.RedisClientDto {
	return this.redisClientDto
}

func (this *IchubRedisClient) SetRedisClientDto(redisClientDto *baseconfig.RedisClientDto) {
	this.redisClientDto = redisClientDto
}

func NewIchubRedis() *IchubRedisClient {
	return &IchubRedisClient{}
}

var redisClientDtoDef = &baseconfig.RedisClientDto{
	Addr:     "192.168.14.58:6379",
	Password: "KiZ9dJBSk1cju",
	DB:       0,
}

func (this *IchubRedisClient) Open() *redis.Client {
	this.Client = redis.NewClient(&redis.Options{
		Addr:     this.redisClientDto.Addr,     // "192.168.14.58:6379",
		Password: this.redisClientDto.Password, //"KiZ9dJBSk1cju",
		DB:       this.redisClientDto.DB,       //
	})
	return this.Client
}

func (this *IchubRedisClient) SetKey(key string, value any) *redis.Client {
	client := this.Client
	client.Set(PREKEY_USER+key, jsonutils.ToJsonStr(value), ExpireTime)
	return this.Client
}

func (this *IchubRedisClient) GetKey(key string) any {
	client := this.Client
	return client.Get(PREKEY_USER + key)
}

func (this *IchubRedisClient) SetKeyInt64(key int64, value any) *redis.Client {

	return this.SetKey(convertor.ToString(key), value)
}

func (this *IchubRedisClient) GetKeyInt64(key int64) any {

	return this.GetKey(PREKEY_USER + convertor.ToString(key))
}

func (this *IchubRedisClient) SetKeyInt32(key int32, value any) *redis.Client {

	return this.SetKey(string(key), value)
}

func (this *IchubRedisClient) GetKeyInt32(key int32) any {
	skey := string(key)
	return this.GetKey(PREKEY_USER + skey)
}
