rule "func-dbquery-rule" "func-dbquery-rule" salience 0
begin

    IchubLog(In)

    id=@id
    Out.SetParam("FuncId","func-dbquery-rule")
    DbResult =  RuleFunc.DbQuery(In)

    Out.SetReturn(200,"计算成功")
    Out.SetParam("DbResult",DbResult)

end