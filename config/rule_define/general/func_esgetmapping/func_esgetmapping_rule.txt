rule "func-esgetmapping-rule" "func-esgetmapping-rule" salience 0
begin
    IchubLog(In)

    id=@id
    Out.SetParam("FuncId","func-esquery-rule")
    Result = RuleFunc.EsGetMapping(In)

    Out.SetReturn(200,"计算成功")
    // Out.SetEsPageResult(Result)
    Out.SetParam("EsResult",Result)

end