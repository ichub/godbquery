rule "func-snowflake-rule" "分布式唯一id:func-snowflake-rule" salience 0
begin


    Out.SetParam("FuncId","func-snowflake")
    id = RuleFunc.SnowflakeNextId()

    Out.SetReturnValue(200)
    Out.SetReturnMsg("计算成功！")
    Out.SetParam("id",RuleFunc.Any2Str(id))

end