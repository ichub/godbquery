package dao

/*
   @Title  文件名称: Employeedao.go
   @Description 描述: 服务Employeedao

   @Author  作者: leijianming@163.com  时间(2024-03-26 19:34:46)
   @Update  作者: leijianming@163.com  时间(2024-03-26 19:34:46)

*/

import (
	"gitee.com/ichub/godbquery/common/base/basedao"
	"gitee.com/ichub/godbquery/common/base/basedto"

	"gitee.com/ichub/godbquery/common/base/pagedto"
	"gitee.com/ichub/godbquery/shop/model"
	"log"
)

var InstEmployeeDAO EmployeeDAO

type EmployeeDAO struct {
	basedao.BaseDao
}

func (this *EmployeeDAO) FindData(result *basedto.IchubResult) *model.Employee {
	return result.Data.(*model.Employee)
}

func (this *EmployeeDAO) FindDataList(result *pagedto.IchubPageResult) *[]model.Employee {
	return result.Data.(*[]model.Employee)
}

/*
@title     函数名称:  Insert
@description      :  新增记录
@auth      作者:      leijianming@163.com 时间: 2024-03-26 19:34:46
@param     输入参数名: entity *model.Employee
@return    返回参数名: int32,error
*/
func (this *EmployeeDAO) Insert(entity *model.Employee) (int32, error) {

	err := this.GetDB().Create(entity).Error
	if err != nil {
		log.Println(err.Error())
		return -1, err
	}

	return entity.Id, err
}

/*
@title     函数名称:  DeleteById
@description      :  根据主键Id 删除记录

@auth      作者:      leijianming@163.com 时间: 2024-03-26 19:34:46
@param     输入参数名: Id int32
@return    返回参数名: error
*/
func (this *EmployeeDAO) DeleteById(Id int32) error {
	var entity model.Employee

	err := this.GetDB().Where("id=?", Id).Delete(&entity).Error
	return err
}

/*
@title     函数名称:  Save
@description      :  保存记录

@auth      作者:      leijianming@163.com 时间: 2024-03-26 19:34:46
@param     输入参数名: entity *model.Employee
@return    返回参数名: int32,error
*/
func (this *EmployeeDAO) Save(entity *model.Employee) (int32, error) {

	err := this.GetDB().Save(entity).Error
	if err != nil {
		log.Println(err.Error())
		return -1, err
	}
	return entity.Id, err
}

/*
@title     函数名称:  Update
@description      :  修改记录

@auth      作者:      leijianming@163.com 时间: 2024-03-26 19:34:46
@param     输入参数名: entity *model.Employee
@return    返回参数名: int32,error
*/
func (this *EmployeeDAO) Update(entity *model.Employee) (int32, error) {

	err := this.GetDB().Model(&model.Employee{}).Where("id=?", entity.Id).Updates(entity).Error
	if err != nil {
		log.Println(err.Error())
		return -1, err
	}
	return entity.Id, err
}

func (this *EmployeeDAO) UpdateNotNull(Id int32, maps map[string]interface{}) (int32, error) {

	err := this.GetDB().Model(&model.Employee{}).Where("id=?", Id).Updates(maps).Error
	return Id, err
}

/*
@title     函数名称:  UpdateMap
@description      :  根据主键Id,map修改指定字段

@auth      作者:      leijianming@163.com 时间: 2024-03-26 19:34:46
@param     输入参数名: Id int64,entity map[string]interface{}
@return    返回参数名: int64,error
*/
func (this *EmployeeDAO) UpdateMap(Id int32, maps map[string]interface{}) (int32, error) {

	err := this.GetDB().Model(&model.Employee{}).Where("id=?", Id).Updates(maps).Error
	return Id, err
}

/*
@title     函数名称:  FindById
@description      :  根据主键Id 查询记录

@auth      作者:      leijianming@163.com 时间: 2024-03-26 19:34:46
@param     输入参数名: Id int32
@return    返回参数名: entity *model.Employee, found bool, err error
*/
func (this *EmployeeDAO) FindById(Id int32) (entity *model.Employee, found bool, err error) {

	entity = new(model.Employee)

	db := this.GetDB().First(entity, Id)
	found = !db.RecordNotFound()
	err = db.Error

	return entity, found, err

}

/*
@title     函数名称:  FindByIds
@description      :  根据主键Id 查询多条记录; FindByIds("1,36,39")

@auth      作者:      leijianming@163.com 时间: 2024-03-26 19:34:46
@param     输入参数名: pks string
@return    返回参数名: *[]model.Employee,error
*/
func (this *EmployeeDAO) FindByIds(pks string) (*[]model.Employee, error) {

	var entities []model.Employee
	db := this.GetDB().Raw("select * from employee where id in (" + pks + ")").Scan(&entities)

	return &entities, db.Error
}

/*
@title     函数名称:  Query
@description      :  查询符合条件的记录！queryParam通用条件分页

@auth      作者:      leijianming@163.com 时间: 2024-03-26 19:34:46
@param     输入参数名: param *pagedto.IchubPageRequest
@return    返回参数名: *pagedto.IchubPageResult
*/
func (this *EmployeeDAO) Query(param *pagedto.IchubPageRequest) *pagedto.IchubPageResult {

	var entities []model.Employee
	return param.Query(&model.Employee{}, &entities)
}

/*
@title     函数名称:  Count
@description      :  计算符合条件的记录总数！IchubPageRequest通用条件

@auth      作者:      leijianming@163.com 时间: 2024-03-26 19:34:46
@param     输入参数名: param *pagedto.IchubPageRequest
@return    返回参数名: int, error
*/
func (this *EmployeeDAO) Count(param *pagedto.IchubPageRequest) (int, error) {

	return param.Count(&model.Employee{})
}
