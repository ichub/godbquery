package dao

import (
	"fmt"
	"gitee.com/ichub/godbquery/common/base/baseutils/jsonutils"
	"gitee.com/ichub/godbquery/common/ichubcontext"
	"gitee.com/ichub/godbquery/shop/dto"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"

	//"github.com/duke-git/lancet/strutil"
	"github.com/jinzhu/gorm"
	"testing"
)

type TestSuite struct {
	suite.Suite
	DB *gorm.DB
}

func TestSuites(t *testing.T) {
	suite.Run(t, new(TestSuite))
}
func (suite *TestSuite) SetupTest() {
	logrus.Info("Suite setup")
	logrus.Info("Before all tests")

	//suite.DB = dbcontent.IniDb("mysql")
	suite.DB = ichubcontext.IchubClient.GetDB()
	defer suite.DB.Close()

}

func (suite *TestSuite) TearDownTest() {
	//	suite.DB.Close()

}

func (suite *TestSuite) Test001_querySysDeptQueryParam() {
	queryParam := &dto.SysDeptQueryParam{}
	queryParam.Ini()
	queryParam.PageSize = 4
	rs, _ := InstSysDeptDAO.FindByQueryParam(queryParam)
	logrus.Info(jsonutils.ToJsonPretty(rs))

}
func (suite *TestSuite) Test002_findByIdSysDept() {

	sysDept, found, _ := InstSysDeptDAO.FindById(102)
	if found {
		fmt.Println(jsonutils.ToJsonPretty(*sysDept))
	}
}
