package dao

/*
   @Title  文件名称: ParamsDao.go
   @Description 描述: DAO层ParamsDao

   @Author  作者: leijianming@163.com  时间(2024-02-08 18:19:47)
   @Update  作者: leijianming@163.com  时间(2024-02-08 18:19:47)

*/

import (
	"fmt"
	"gitee.com/ichub/godbquery/common/base/baseconsts"
	basemodel "gitee.com/ichub/godbquery/common/base/basemodel"
	"gitee.com/ichub/godbquery/common/dbcontent"
	"gitee.com/ichub/godbquery/shop/dto"
	"gitee.com/ichub/godbquery/shop/model"
	"github.com/jinzhu/gorm"
	"log"
	"strings"
	"time"
)

// const PAGE_SIZE_DEFAULT = 500
// const PAGE_SIZE_MAX = 1000
type ParamsDAO struct {
}

var InstParamsDAO ParamsDAO

/*
@title     函数名称:  Insert
@description      :  新增记录
@auth      作者:      leijianming@163.com 时间: 2024-02-08 18:19:47
@param     输入参数名: entity *model.Params
@return    返回参数名: int64,error
*/
func (daoInst *ParamsDAO) Insert(entity *model.Params) (int64, error) {

	err := dbcontent.GetDB().Create(entity).Error
	if err != nil {
		log.Println(err.Error())
		return -1, err
	}

	return entity.Id, err
}

/*
@title     函数名称:  DeleteById
@description      :  根据主键Id 删除记录

@auth      作者:      leijianming@163.com 时间: 2024-02-08 18:19:47
@param     输入参数名: Id int64
@return    返回参数名: error
*/
func (daoInst *ParamsDAO) DeleteById(Id int64) error {
	var entity model.Params

	err := dbcontent.GetDB().Where("id=?", Id).Delete(&entity).Error
	return err
}

/*
@title     函数名称:  Save
@description      :  保存记录

@auth      作者:      leijianming@163.com 时间: 2024-02-08 18:19:47
@param     输入参数名: entity *model.Params
@return    返回参数名: int64,error
*/
func (daoInst *ParamsDAO) Save(entity *model.Params) (int64, error) {

	err := dbcontent.GetDB().Save(entity).Error
	if err != nil {
		log.Println(err.Error())
		return -1, err
	}
	return entity.Id, err
}

/*
@title     函数名称:  Update
@description      :  修改记录

@auth      作者:      leijianming@163.com 时间: 2024-02-08 18:19:47
@param     输入参数名: entity *model.Params
@return    返回参数名: int64,error
*/
func (daoInst *ParamsDAO) Update(entity *model.Params) (int64, error) {

	err := dbcontent.GetDB().Model(&model.Params{}).Where("id=?", entity.Id).Updates(entity).Error
	if err != nil {
		log.Println(err.Error())
		return -1, err
	}
	return entity.Id, err
}

/*
@title     函数名称:  UpdateNotNullProps
@description      :  根据主键Id修改非nil字段

@auth      作者:      leijianming@163.com 时间: 2024-02-08 18:19:47
@param     输入参数名: Id int64,entity map[string]interface{}
@return    返回参数名: int64,error
*/
func (daoInst *ParamsDAO) UpdateNotNullProps(Id int64, maps map[string]interface{}) (int64, error) {

	err := dbcontent.GetDB().Model(&model.Params{}).Where("id=?", Id).Updates(maps).Error
	return Id, err
}

/*
@title     函数名称:  UpdateMap
@description      :  根据主键Id,map修改指定字段

@auth      作者:      leijianming@163.com 时间: 2024-02-08 18:19:47
@param     输入参数名: Id int64,entity map[string]interface{}
@return    返回参数名: int64,error
*/
func (daoInst *ParamsDAO) UpdateMap(Id int64, maps map[string]interface{}) (int64, error) {

	err := dbcontent.GetDB().Model(&model.Params{}).Where("id=?", Id).Updates(maps).Error
	return Id, err
}

/*
@title     函数名称:  FindById
@description      :  根据主键Id 查询记录

@auth      作者:      leijianming@163.com 时间: 2024-02-08 18:19:47
@param     输入参数名: Id int64
@return    返回参数名: entity *model.Params, found bool, err error
*/
func (daoInst *ParamsDAO) FindById(Id int64) (entity *model.Params, found bool, err error) {

	entity = new(model.Params)

	db := dbcontent.GetDB().First(entity, Id)
	found = !db.RecordNotFound()
	err = db.Error

	return entity, found, err

}

/*
@title     函数名称:  FindByIds
@description      :  根据主键Id 查询多条记录; FindByIds("1,36,39")

@auth      作者:      leijianming@163.com 时间: 2024-02-08 18:19:47
@param     输入参数名: pks string
@return    返回参数名: *[]model.Params,error
*/
func (daoInst *ParamsDAO) FindByIds(pks string) (*[]model.Params, error) {

	var entities []model.Params
	db := dbcontent.GetDB().Raw("select * from params where id in (" + pks + ")").Scan(&entities)

	return &entities, db.Error
}

/*
@title     函数名称:  FindByQueryParam
@description      :  查询符合条件的记录！queryParam通用条件分页

@auth      作者:      leijianming@163.com 时间: 2024-02-08 18:19:47
@param     输入参数名: param *dto.ParamsQueryParam
@return    返回参数名: *[]model.Params,error
*/
func (daoInst *ParamsDAO) FindByQueryParam(param *dto.ParamsQueryParam) (*[]model.Params, error) {

	var entities []model.Params
	limit := param.PageSize
	if limit <= baseconsts.PAGE_SIZE_ZERO {

		limit = baseconsts.PAGE_SIZE_DEFAULT

	} else if limit > baseconsts.PAGE_SIZE_MAX {

		limit = baseconsts.PAGE_SIZE_MAX

	}
	param.PageSize = limit
	if param.Current <= 0 {
		param.Current = 1
	}
	start := (param.Current - 1) * limit

	dbc := dbcontent.GetDB().Model(&model.Params{}).Offset(start).Limit(param.PageSize)
	dbc = daoInst.buildWhere(param, dbc)

	if len(param.OrderBys) > 0 {
		rs := strings.Split(param.OrderBys, ",")

		for _, value := range rs {
			orders := strings.Split(value, "|")
			dbc = dbc.Order(strings.Join(orders, " "))
		}
	}

	err := dbc.Find(&entities).Error
	return &entities, err
}

/*
@title     函数名称:  CountByQueryParam
@description      :  计算符合条件的记录总数！queryParam通用条件

@auth      作者:      leijianming@163.com 时间: 2024-02-08 18:19:47
@param     输入参数名: param *dto.ParamsQueryParam
@return    返回参数名: int32, error
*/
func (daoInst *ParamsDAO) CountByQueryParam(param *dto.ParamsQueryParam) (int, error) {

	var count int
	dbc := dbcontent.GetDB().Model(&model.Params{}).Offset(0).Limit(1)
	dbc = daoInst.buildWhere(param, dbc)

	err := dbc.Count(&count).Error

	return count, err
}

func (daoInst *ParamsDAO) formatTime(sec int64) string {
	timeLayout := "2006-01-02 15:04:05"
	datetime := time.Unix(sec, 0).Format(timeLayout)
	return datetime
}

func (daoInst *ParamsDAO) formatDate(sec int64) string {
	timeLayout := "2006-01-02"
	datetime := time.Unix(sec, 0).Format(timeLayout)
	return datetime
}

func (daoInst *ParamsDAO) localTimeFormat(ptime time.Time) string {
	timeLayout := "2006-01-02 15:04:05"
	return ptime.Format(timeLayout)

}

func (daoInst *ParamsDAO) localTimeUTCFormat(ptime time.Time) string {
	timeLayout := "2006-01-02 15:04:05"
	return ptime.UTC().Format(timeLayout)

}

func (daoInst *ParamsDAO) localDateFormat(localDateInt basemodel.LocalDateInt) string {
	timeLayout := "2006-01-02"
	datetime := localDateInt.Time.Format(timeLayout)
	return datetime
}

func (daoInst *ParamsDAO) localDatetimeFormat(localTimeInt basemodel.LocalTimeInt) string {
	timeLayout := "2006-01-02 15:04:05"
	datetime := localTimeInt.Time.Format(timeLayout)
	return datetime
}

/*
@title     函数名称:  buildWhere
@description      :  构造通用查询条件

@auth      作者:      leijianming@163.com 时间: 2024-02-08 18:19:47
@param     输入参数名: param * dto.ParamsQueryParam, dbcontent *gorm.D
@return    返回参数名: *gorm.DB
*/
func (daoInst *ParamsDAO) buildWhere(param *dto.ParamsQueryParam, dbc *gorm.DB) *gorm.DB {

	// 区间条件 时间，数字等

	pp := param.Param
	if pp == nil {
		return dbc
	}
	if pp.DateRanges != nil && len(pp.DateRanges) > 0 {
		for key := range pp.DateRanges { //取map中的数组值
			dbc = dbc.Where(fmt.Sprintf("%s BETWEEN ? and ?", key),
				daoInst.formatTime(pp.DateRanges[key][0]), daoInst.formatTime(pp.DateRanges[key][1]))
		}
	}

	if pp.IntRanges != nil && len(pp.IntRanges) > 0 {
		for key := range pp.IntRanges { //取map中的数组值
			dbc = dbc.Where(fmt.Sprintf("%s BETWEEN ? and ?", key), pp.IntRanges[key][0], pp.IntRanges[key][1])
		}
	}

	if pp.StringRanges != nil && len(pp.StringRanges) > 0 {
		for key := range pp.StringRanges { //取map中的数组值
			dbc = dbc.Where(fmt.Sprintf("%s BETWEEN ? and ?", key), pp.StringRanges[key][0], pp.StringRanges[key][1])
		}
	}

	// id in (1,2,3)
	if pp.InRanges != nil && len(pp.InRanges) > 0 {
		for key := range pp.InRanges { //取map中的数组值
			dbc = dbc.Where(fmt.Sprintf("%s in (%s)", key, pp.InRanges[key]))
		}
	}
	/*if pp.DatetimeRanges != nil && len(pp.DatetimeRanges) > 0 {
		for key := range pp.DatetimeRanges { //取map中的数组值
			dbc = dbc.Where(fmt.Sprintf("%s BETWEEN ? and ?", key),
				pp.DatetimeRanges[key][0].Format("2006-01-02 15:04:05"),
				pp.DatetimeRanges[key][1].Format("2006-01-02 15:04:05"))
		}
	}*/
	// 等于条件 或者未来支持模糊查询！

	if pp.Id != 0 {
		dbc = dbc.Where("id=?", pp.Id)
	}

	if pp.ParamType != "" {

		if param.FuzzyQuery {
			dbc = dbc.Where("param_type like ?", "%"+pp.ParamType+"%")
		} else {
			dbc = dbc.Where("param_type=?", pp.ParamType)
		}

	}

	if pp.ParamName != "" {

		if param.FuzzyQuery {
			dbc = dbc.Where("param_name like ?", "%"+pp.ParamName+"%")
		} else {
			dbc = dbc.Where("param_name=?", pp.ParamName)
		}

	}

	if pp.ParentId != 0 {
		dbc = dbc.Where("parent_id=?", pp.ParentId)
	}

	return dbc

}

/*
@title     函数名称:  Demo
@description      :  代码调用示例

@auth      作者:      leijianming@163.com 时间: 2024-02-08 18:19:47
@param     输入参数名: 无
@return    返回参数名: 无
*/
func (daoInst *ParamsDAO) Demo() {
	//var InstParamsDAO = dao.InstParamsDAO
	entity, _, _ := InstParamsDAO.FindById(3)
	fmt.Println(*entity)

	var qp dto.ParamsQueryParam
	qp.Param = new(dto.ParamsParam)
	qp.Ini()

	entities, _ := InstParamsDAO.FindByQueryParam(&qp)
	/*for _, v := range *entities {
		if v.Name != nil {
			fmt.Println(v.String())
		}
	}*/
	fmt.Println(len(*entities))
	fmt.Println(qp.String())

	var sentity model.Params
	sentity.Ini(false)
	id, _ := InstParamsDAO.Insert(&sentity)
	fmt.Println(id)

	ret, _, _ := InstParamsDAO.FindById(entity.Id)
	fmt.Println(ret.String())

	entity.Id = sentity.Id
	InstParamsDAO.Update(entity)

	InstParamsDAO.DeleteById(entity.Id)

}
