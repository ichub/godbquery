package dao

/*
   @Title  文件名称: RulesDao.go
   @Description 描述: DAO层RulesDao

   @Author  作者: leijianming@163.com  时间(2024-02-08 20:08:55)
   @Update  作者: leijianming@163.com  时间(2024-02-08 20:08:55)

*/

import (
	"fmt"
	"gitee.com/ichub/godbquery/common/base/baseconsts"
	basemodel "gitee.com/ichub/godbquery/common/base/basemodel"
	"gitee.com/ichub/godbquery/common/base/pagedto"
	"gitee.com/ichub/godbquery/common/dbcontent"
	"gitee.com/ichub/godbquery/shop/dto"
	"gitee.com/ichub/godbquery/shop/model"
	"github.com/jinzhu/gorm"
	//"strconv"
	"log"
	"strings"
	"time"
)

type RulesDAO struct {
}

func NewRulesDAO() *RulesDAO {
	return &RulesDAO{}
}

var InstRulesDAO RulesDAO

/*
@title     函数名称:  Insert
@description      :  新增记录
@auth      作者:      leijianming@163.com 时间: 2024-02-08 20:08:55
@param     输入参数名: entity *model.Rules
@return    返回参数名: int64,error
*/
func (daoInst *RulesDAO) Insert(entity *model.Rules) (int64, error) {

	err := dbcontent.GetDB().Create(entity).Error
	if err != nil {
		log.Println(err.Error())
		return -1, err
	}

	return entity.RuleId, err
}

/*
@title     函数名称:  DeleteById
@description      :  根据主键RuleId 删除记录

@auth      作者:      leijianming@163.com 时间: 2024-02-08 20:08:55
@param     输入参数名: RuleId int64
@return    返回参数名: error
*/
func (daoInst *RulesDAO) DeleteById(RuleId int64) error {
	var entity model.Rules

	err := dbcontent.GetDB().Where("rule_id=?", RuleId).Delete(&entity).Error
	return err
}

/*
@title     函数名称:  Save
@description      :  保存记录

@auth      作者:      leijianming@163.com 时间: 2024-02-08 20:08:55
@param     输入参数名: entity *model.Rules
@return    返回参数名: int64,error
*/
func (daoInst *RulesDAO) Save(entity *model.Rules) (int64, error) {

	err := dbcontent.GetDB().Save(entity).Error
	if err != nil {
		log.Println(err.Error())
		return -1, err
	}
	return entity.RuleId, err
}

/*
@title     函数名称:  Update
@description      :  修改记录

@auth      作者:      leijianming@163.com 时间: 2024-02-08 20:08:55
@param     输入参数名: entity *model.Rules
@return    返回参数名: int64,error
*/
func (daoInst *RulesDAO) Update(entity *model.Rules) (int64, error) {

	err := dbcontent.GetDB().Model(&model.Rules{}).Where("rule_id=?", entity.RuleId).Updates(entity).Error
	if err != nil {
		log.Println(err.Error())
		return -1, err
	}
	return entity.RuleId, err
}

/*
@title     函数名称:  UpdateNotNullProps
@description      :  根据主键RuleId修改非nil字段

@auth      作者:      leijianming@163.com 时间: 2024-02-08 20:08:55
@param     输入参数名: Id int64,entity map[string]interface{}
@return    返回参数名: int64,error
*/
func (daoInst *RulesDAO) UpdateNotNullProps(RuleId int64, maps map[string]interface{}) (int64, error) {

	err := dbcontent.GetDB().Model(&model.Rules{}).Where("rule_id=?", RuleId).Updates(maps).Error
	return RuleId, err
}

/*
@title     函数名称:  UpdateMap
@description      :  根据主键RuleId,map修改指定字段

@auth      作者:      leijianming@163.com 时间: 2024-02-08 20:08:55
@param     输入参数名: Id int64,entity map[string]interface{}
@return    返回参数名: int64,error
*/
func (daoInst *RulesDAO) UpdateMap(RuleId int64, maps map[string]interface{}) (int64, error) {

	err := dbcontent.GetDB().Model(&model.Rules{}).Where("rule_id=?", RuleId).Updates(maps).Error
	return RuleId, err
}

/*
@title     函数名称:  FindById
@description      :  根据主键RuleId 查询记录

@auth      作者:      leijianming@163.com 时间: 2024-02-08 20:08:55
@param     输入参数名: RuleId int64
@return    返回参数名: entity *model.Rules, found bool, err error
*/
func (daoInst *RulesDAO) FindById(RuleId int64) (entity *model.Rules, found bool, err error) {

	entity = new(model.Rules)

	db := dbcontent.GetDB().First(entity, RuleId)
	found = !db.RecordNotFound()
	err = db.Error

	return entity, found, err

}

/*
@title     函数名称:  FindByIds
@description      :  根据主键RuleId 查询多条记录; FindByIds("1,36,39")

@auth      作者:      leijianming@163.com 时间: 2024-02-08 20:08:55
@param     输入参数名: pks string
@return    返回参数名: *[]model.Rules,error
*/
func (daoInst *RulesDAO) FindByIds(pks string) (*[]model.Rules, error) {

	var entities []model.Rules
	db := dbcontent.GetDB().Raw("select * from rules where rule_id in (" + pks + ")").Scan(&entities)

	return &entities, db.Error
}

/*
@title     函数名称:  FindByQueryParam
@description      :  查询符合条件的记录！queryParam通用条件分页

@auth      作者:      leijianming@163.com 时间: 2024-02-08 20:08:55
@param     输入参数名: param *dto.RulesQueryParam
@return    返回参数名: *[]model.Rules,error
*/
func (daoInst *RulesDAO) FindByQueryParam(param *dto.RulesQueryParam) (*[]model.Rules, error) {

	var entities []model.Rules
	limit := param.PageSize
	if limit <= baseconsts.PAGE_SIZE_ZERO {

		limit = baseconsts.PAGE_SIZE_DEFAULT

	} else if limit > baseconsts.PAGE_SIZE_MAX {

		limit = baseconsts.PAGE_SIZE_MAX

	}
	param.PageSize = limit
	if param.Current <= 0 {
		param.Current = 1
	}
	start := (param.Current - 1) * limit

	dbc := dbcontent.GetDB().Model(&model.Rules{}).Offset(start).Limit(param.PageSize)
	dbc = daoInst.buildWhere(param, dbc)

	if len(param.OrderBys) > 0 {
		rs := strings.Split(param.OrderBys, ",")

		for _, value := range rs {
			orders := strings.Split(value, "|")
			dbc = dbc.Order(strings.Join(orders, " "))
		}
	}

	err := dbc.Find(&entities).Error
	return &entities, err
}
func (daoInst *RulesDAO) Query(req *pagedto.IchubPageRequest) *pagedto.IchubPageResult {

	var entities []model.Rules

	return req.Query(&model.Rules{}, &entities)

}
func (daoInst *RulesDAO) Count(param *pagedto.IchubPageRequest) (int, error) {
	var entity model.Rules

	count, err := param.Count(&entity)

	return count, err
}

/*
@title     函数名称:  CountByQueryParam
@description      :  计算符合条件的记录总数！queryParam通用条件

@auth      作者:      leijianming@163.com 时间: 2024-02-08 20:08:55
@param     输入参数名: param *dto.RulesQueryParam
@return    返回参数名: int32, error
*/
func (daoInst *RulesDAO) CountByQueryParam(param *dto.RulesQueryParam) (int, error) {

	var count int
	dbc := dbcontent.GetDB().Model(&model.Rules{}).Offset(0).Limit(1)
	dbc = daoInst.buildWhere(param, dbc)

	err := dbc.Count(&count).Error

	return count, err
}

func (daoInst *RulesDAO) formatTime(sec int64) string {
	timeLayout := "2006-01-02 15:04:05"
	datetime := time.Unix(sec, 0).Format(timeLayout)
	return datetime
}

func (daoInst *RulesDAO) formatDate(sec int64) string {
	timeLayout := "2006-01-02"
	datetime := time.Unix(sec, 0).Format(timeLayout)
	return datetime
}

func (daoInst *RulesDAO) localTimeFormat(ptime time.Time) string {
	timeLayout := "2006-01-02 15:04:05"
	return ptime.Format(timeLayout)

}

func (daoInst *RulesDAO) localTimeUTCFormat(ptime time.Time) string {
	timeLayout := "2006-01-02 15:04:05"
	return ptime.UTC().Format(timeLayout)

}

func (daoInst *RulesDAO) localDateFormat(localDateInt basemodel.LocalDateInt) string {
	timeLayout := "2006-01-02"
	datetime := localDateInt.Time.Format(timeLayout)
	return datetime
}

func (daoInst *RulesDAO) localDatetimeFormat(localTimeInt basemodel.LocalTimeInt) string {
	timeLayout := "2006-01-02 15:04:05"
	datetime := localTimeInt.Time.Format(timeLayout)
	return datetime
}

/*
@title     函数名称:  buildWhere
@description      :  构造通用查询条件

@auth      作者:      leijianming@163.com 时间: 2024-02-08 20:08:55
@param     输入参数名: param * dto.RulesQueryParam, dbcontent *gorm.D
@return    返回参数名: *gorm.DB
*/
func (daoInst *RulesDAO) buildWhere(param *dto.RulesQueryParam, dbc *gorm.DB) *gorm.DB {

	// 区间条件 时间，数字等

	pp := param.Param
	if pp.DateRanges != nil && len(pp.DateRanges) > 0 {
		for key := range pp.DateRanges { //取map中的数组值
			dbc = dbc.Where(fmt.Sprintf("%s BETWEEN ? and ?", key),
				daoInst.formatTime(pp.DateRanges[key][0]), daoInst.formatTime(pp.DateRanges[key][1]))
		}
	}

	if pp.IntRanges != nil && len(pp.IntRanges) > 0 {
		for key := range pp.IntRanges { //取map中的数组值
			dbc = dbc.Where(fmt.Sprintf("%s BETWEEN ? and ?", key), pp.IntRanges[key][0], pp.IntRanges[key][1])
		}
	}

	if pp.StringRanges != nil && len(pp.StringRanges) > 0 {
		for key := range pp.StringRanges { //取map中的数组值
			dbc = dbc.Where(fmt.Sprintf("%s BETWEEN ? and ?", key), pp.StringRanges[key][0], pp.StringRanges[key][1])
		}
	}

	// id in (1,2,3)
	if pp.InRanges != nil && len(pp.InRanges) > 0 {
		for key := range pp.InRanges { //取map中的数组值
			dbc = dbc.Where(fmt.Sprintf("%s in (%s)", key, pp.InRanges[key]))
		}
	}
	/*if pp.DatetimeRanges != nil && len(pp.DatetimeRanges) > 0 {
		for key := range pp.DatetimeRanges { //取map中的数组值
			dbc = dbc.Where(fmt.Sprintf("%s BETWEEN ? and ?", key),
				pp.DatetimeRanges[key][0].Format("2006-01-02 15:04:05"),
				pp.DatetimeRanges[key][1].Format("2006-01-02 15:04:05"))
		}
	}*/
	// 等于条件 或者未来支持模糊查询！

	if pp.RuleId != 0 {
		dbc = dbc.Where("rule_id=?", pp.RuleId)
	}

	if pp.RuleKey != "" {

		if param.FuzzyQuery {
			dbc = dbc.Where("rule_key like '" + "%" + pp.RuleKey + "%'")
		} else {
			dbc = dbc.Where("rule_key =?", pp.RuleKey)
		}

	}

	if pp.Domain != "" {

		if param.FuzzyQuery {
			dbc = dbc.Where("domain like ?", "%"+pp.Domain+"%")
		} else {
			dbc = dbc.Where("domain=?", pp.Domain)
		}

	}

	if pp.Rule != "" {

		if param.FuzzyQuery {
			dbc = dbc.Where("rule_text like ?", "%"+pp.Rule+"%")
		} else {
			dbc = dbc.Where("rule_text=?", pp.Rule)
		}

	}

	if pp.Param != "" {

		if param.FuzzyQuery {
			dbc = dbc.Where("params like ?", "'%"+pp.Param+"%'")
		} else {
			dbc = dbc.Where("params=?", pp.Param)
		}

	}

	if pp.Remark != "" {

		if param.FuzzyQuery {
			dbc = dbc.Where("remark like ?", "%"+pp.Remark+"%")
		} else {
			dbc = dbc.Where("remark=?", pp.Remark)
		}

	}

	return dbc

}

/*
@title     函数名称:  Demo
@description      :  代码调用示例

@auth      作者:      leijianming@163.com 时间: 2024-02-08 20:08:55
@param     输入参数名: 无
@return    返回参数名: 无
*/
func (daoInst *RulesDAO) Demo() {
	//var InstRulesDAO = dao.InstRulesDAO
	entity, _, _ := InstRulesDAO.FindById(3)
	fmt.Println(*entity)

	var qp dto.RulesQueryParam
	qp.Param = new(dto.RulesParam)
	qp.Ini()

	entities, _ := InstRulesDAO.FindByQueryParam(&qp)
	/*for _, v := range *entities {
		if v.Name != nil {
			fmt.Println(v.String())
		}
	}*/
	fmt.Println(len(*entities))
	fmt.Println(qp.String())

	var sentity model.Rules
	sentity.Ini(false)
	id, _ := InstRulesDAO.Insert(&sentity)
	fmt.Println(id)

	ret, _, _ := InstRulesDAO.FindById(entity.RuleId)
	fmt.Println(ret.String())

	entity.RuleId = sentity.RuleId
	InstRulesDAO.Update(entity)

	InstRulesDAO.DeleteById(entity.RuleId)

}
