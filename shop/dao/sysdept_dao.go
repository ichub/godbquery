package dao

/*
   @Title  文件名称: SysDeptDao.go
   @Description 描述: DAO层SysDeptDao

   @Author  作者: leijianming@163.com  时间(2024-01-24 17:15:32)
   @Update  作者: leijianming@163.com  时间(2024-01-24 17:15:32)

*/

import (
	"fmt"
	"gitee.com/ichub/godbquery/common/base/baseconsts"
	"gitee.com/ichub/godbquery/common/base/basemodel"
	"gitee.com/ichub/godbquery/common/dbcontent"
	"gitee.com/ichub/godbquery/shop/dto"
	"gitee.com/ichub/godbquery/shop/model"
	"github.com/jinzhu/gorm"
	"log"
	"strings"
	"time"
)

// const PAGE_SIZE_DEFAULT = 500
// const PAGE_SIZE_MAX = 1000
type SysDeptDAO struct {
}

var InstSysDeptDAO SysDeptDAO

/*
@title     函数名称:  Insert
@description      :  新增记录
@auth      作者:      leijianming@163.com 时间: 2024-01-24 17:15:32
@param     输入参数名: entity *basemodel.SysDept
@return    返回参数名: int64,error
*/
func (daoInst *SysDeptDAO) Insert(entity *model.SysDept) (int64, error) {

	err := dbcontent.GetDB().Create(entity).Error
	if err != nil {
		log.Println(err.Error())
		return -1, err
	}

	return entity.DeptId, err
}

/*
@title     函数名称:  DeleteById
@description      :  根据主键DeptId 删除记录

@auth      作者:      leijianming@163.com 时间: 2024-01-24 17:15:32
@param     输入参数名: DeptId int64
@return    返回参数名: error
*/
func (daoInst *SysDeptDAO) DeleteById(DeptId int64) error {
	var entity model.SysDept

	err := dbcontent.GetDB().Where("dept_id=?", DeptId).Delete(&entity).Error
	return err
}

/*
@title     函数名称:  Save
@description      :  保存记录

@auth      作者:      leijianming@163.com 时间: 2024-01-24 17:15:32
@param     输入参数名: entity *basemodel.SysDept
@return    返回参数名: int64,error
*/
func (daoInst *SysDeptDAO) Save(entity *model.SysDept) (int64, error) {

	err := dbcontent.GetDB().Save(entity).Error
	if err != nil {
		log.Println(err.Error())
		return -1, err
	}
	return entity.DeptId, err
}

/*
@title     函数名称:  Update
@description      :  修改记录

@auth      作者:      leijianming@163.com 时间: 2024-01-24 17:15:32
@param     输入参数名: entity *basemodel.SysDept
@return    返回参数名: int64,error
*/
func (daoInst *SysDeptDAO) Update(entity *model.SysDept) (int64, error) {

	err := dbcontent.GetDB().Model(&model.SysDept{}).Where("dept_id=?", entity.DeptId).Updates(entity).Error
	if err != nil {
		log.Println(err.Error())
		return -1, err
	}
	return entity.DeptId, err
}

/*
@title     函数名称:  UpdateNotNullProps
@description      :  根据主键DeptId修改非nil字段

@auth      作者:      leijianming@163.com 时间: 2024-01-24 17:15:32
@param     输入参数名: Id int64,entity map[string]interface{}
@return    返回参数名: int64,error
*/
func (daoInst *SysDeptDAO) UpdateNotNullProps(DeptId int64, maps map[string]interface{}) (int64, error) {

	err := dbcontent.GetDB().Model(&model.SysDept{}).Where("dept_id=?", DeptId).Updates(maps).Error
	return 0, err
}

/*
@title     函数名称:  UpdateMap
@description      :  根据主键DeptId,map修改指定字段

@auth      作者:      leijianming@163.com 时间: 2024-01-24 17:15:32
@param     输入参数名: Id int64,entity map[string]interface{}
@return    返回参数名: int64,error
*/
func (daoInst *SysDeptDAO) UpdateMap(DeptId int64, maps map[string]interface{}) (int64, error) {

	err := dbcontent.GetDB().Model(&model.SysDept{}).Where("dept_id=?", DeptId).Updates(maps).Error
	return 0, err
}

/*
@title     函数名称:  FindById
@description      :  根据主键DeptId 查询记录

@auth      作者:      leijianming@163.com 时间: 2024-01-24 17:15:32
@param     输入参数名: DeptId int64
@return    返回参数名: entity *basemodel.SysDept, found bool, err error
*/
func (daoInst *SysDeptDAO) FindById(DeptId int64) (entity *model.SysDept, found bool, err error) {

	entity = new(model.SysDept)

	db := dbcontent.GetDB().First(entity, DeptId)
	found = !db.RecordNotFound()
	err = db.Error

	return entity, found, err

}

/*
@title     函数名称:  FindByIds
@description      :  根据主键DeptId 查询多条记录; FindByIds("1,36,39")

@auth      作者:      leijianming@163.com 时间: 2024-01-24 17:15:32
@param     输入参数名: pks string
@return    返回参数名: *[]basemodel.SysDept,error
*/
func (daoInst *SysDeptDAO) FindByIds(pks string) (*[]model.SysDept, error) {

	var entities []model.SysDept
	db := dbcontent.GetDB().Raw("select * from sys_dept where dept_id in (" + pks + ")").Scan(&entities)

	return &entities, db.Error
}

/*
@title     函数名称:  FindByQueryParam
@description      :  查询符合条件的记录！queryParam通用条件分页

@auth      作者:      leijianming@163.com 时间: 2024-01-24 17:15:32
@param     输入参数名: param *basedto.SysDeptQueryParam
@return    返回参数名: *[]basemodel.SysDept,error
*/
func (daoInst *SysDeptDAO) FindByQueryParam(param *dto.SysDeptQueryParam) (*[]model.SysDept, error) {

	var entities []model.SysDept
	limit := param.PageSize
	if limit <= baseconsts.PAGE_SIZE_ZERO {

		limit = baseconsts.PAGE_SIZE_DEFAULT

	} else if limit > baseconsts.PAGE_SIZE_MAX {

		limit = baseconsts.PAGE_SIZE_MAX

	}
	param.PageSize = limit
	if param.Current <= 0 {
		param.Current = 1
	}
	start := (param.Current - 1) * limit

	dbc := dbcontent.GetDB().Model(&model.SysDept{}).Offset(start).Limit(param.PageSize)
	dbc = daoInst.buildWhere(param, dbc)

	if len(param.OrderBys) > 0 {
		rs := strings.Split(param.OrderBys, ",")

		for _, value := range rs {
			orders := strings.Split(value, "|")
			dbc = dbc.Order(strings.Join(orders, " "))
		}
	}

	err := dbc.Find(&entities).Error
	return &entities, err
}

/*
@title     函数名称:  CountByQueryParam
@description      :  计算符合条件的记录总数！queryParam通用条件

@auth      作者:      leijianming@163.com 时间: 2024-01-24 17:15:32
@param     输入参数名: param *basedto.SysDeptQueryParam
@return    返回参数名: int32, error
*/
func (daoInst *SysDeptDAO) CountByQueryParam(param *dto.SysDeptQueryParam) (int, error) {

	var count int
	dbc := dbcontent.GetDB().Model(&model.SysDept{}).Offset(0).Limit(1)
	dbc = daoInst.buildWhere(param, dbc)

	err := dbc.Count(&count).Error

	return count, err
}

func (daoInst *SysDeptDAO) formatTime(sec int64) string {
	timeLayout := "2006-01-02 15:04:05"
	datetime := time.Unix(sec, 0).Format(timeLayout)
	return datetime
}

func (daoInst *SysDeptDAO) formatDate(sec int64) string {
	timeLayout := "2006-01-02"
	datetime := time.Unix(sec, 0).Format(timeLayout)
	return datetime
}

func (daoInst *SysDeptDAO) localTimeFormat(ptime time.Time) string {
	timeLayout := "2006-01-02 15:04:05"
	return ptime.Format(timeLayout)

}

func (daoInst *SysDeptDAO) localTimeUTCFormat(ptime time.Time) string {
	timeLayout := "2006-01-02 15:04:05"
	return ptime.UTC().Format(timeLayout)

}

func (daoInst *SysDeptDAO) localDateFormat(localDateInt basemodel.LocalDateInt) string {
	timeLayout := "2006-01-02"
	datetime := localDateInt.Time.Format(timeLayout)
	return datetime
}

func (daoInst *SysDeptDAO) localDatetimeFormat(localTimeInt basemodel.LocalTimeInt) string {
	timeLayout := "2006-01-02 15:04:05"
	datetime := localTimeInt.Time.Format(timeLayout)
	return datetime
}

/*
@title     函数名称:  buildWhere
@description      :  构造通用查询条件

@auth      作者:      leijianming@163.com 时间: 2024-01-24 17:15:32
@param     输入参数名: param * basedto.SysDeptQueryParam, dbcontent *gorm.D
@return    返回参数名: *gorm.DB
*/
func (daoInst *SysDeptDAO) buildWhere(param *dto.SysDeptQueryParam, dbc *gorm.DB) *gorm.DB {

	// 区间条件 时间，数字等

	pp := param.Param
	if pp.DateRanges != nil && len(pp.DateRanges) > 0 {
		for key := range pp.DateRanges { //取map中的数组值
			dbc = dbc.Where(fmt.Sprintf("%s BETWEEN ? and ?", key),
				daoInst.formatTime(pp.DateRanges[key][0]), daoInst.formatTime(pp.DateRanges[key][1]))
		}
	}

	if pp.IntRanges != nil && len(pp.IntRanges) > 0 {
		for key := range pp.IntRanges { //取map中的数组值
			dbc = dbc.Where(fmt.Sprintf("%s BETWEEN ? and ?", key), pp.IntRanges[key][0], pp.IntRanges[key][1])
		}
	}

	if pp.StringRanges != nil && len(pp.StringRanges) > 0 {
		for key := range pp.StringRanges { //取map中的数组值
			dbc = dbc.Where(fmt.Sprintf("%s BETWEEN ? and ?", key), pp.StringRanges[key][0], pp.StringRanges[key][1])
		}
	}

	// id in (1,2,3)
	if pp.InRanges != nil && len(pp.InRanges) > 0 {
		for key := range pp.InRanges { //取map中的数组值
			dbc = dbc.Where(fmt.Sprintf("%s in (%s)", key, pp.InRanges[key]))
		}
	}
	/*if pp.DatetimeRanges != nil && len(pp.DatetimeRanges) > 0 {
		for key := range pp.DatetimeRanges { //取map中的数组值
			dbc = dbc.Where(fmt.Sprintf("%s BETWEEN ? and ?", key),
				pp.DatetimeRanges[key][0].Format("2006-01-02 15:04:05"),
				pp.DatetimeRanges[key][1].Format("2006-01-02 15:04:05"))
		}
	}*/
	// 等于条件 或者未来支持模糊查询！

	if pp.DeptId != 0 {
		dbc = dbc.Where("dept_id=?", pp.DeptId)
	}

	if pp.ParentId != 0 {
		dbc = dbc.Where("parent_id=?", pp.ParentId)
	}

	if pp.Ancestors != "" {

		if param.FuzzyQuery {
			dbc = dbc.Where("ancestors like ?", "%"+pp.Ancestors+"%")
		} else {
			dbc = dbc.Where("ancestors=?", pp.Ancestors)
		}

	}

	if pp.DeptName != "" {

		if param.FuzzyQuery {
			dbc = dbc.Where("dept_name like ?", "%"+pp.DeptName+"%")
		} else {
			dbc = dbc.Where("dept_name=?", pp.DeptName)
		}

	}

	if pp.OrderNum != 0 {
		dbc = dbc.Where("order_num=?", pp.OrderNum)
	}

	if pp.Leader != "" {

		if param.FuzzyQuery {
			dbc = dbc.Where("leader like ?", "%"+pp.Leader+"%")
		} else {
			dbc = dbc.Where("leader=?", pp.Leader)
		}

	}

	if pp.Phone != "" {

		if param.FuzzyQuery {
			dbc = dbc.Where("phone like ?", "%"+pp.Phone+"%")
		} else {
			dbc = dbc.Where("phone=?", pp.Phone)
		}

	}

	if pp.Email != "" {

		if param.FuzzyQuery {
			dbc = dbc.Where("email like ?", "%"+pp.Email+"%")
		} else {
			dbc = dbc.Where("email=?", pp.Email)
		}

	}

	if pp.Status != "" {

		if param.FuzzyQuery {
			dbc = dbc.Where("status like ?", "%"+pp.Status+"%")
		} else {
			dbc = dbc.Where("status=?", pp.Status)
		}

	}

	if pp.DelFlag != "" {

		if param.FuzzyQuery {
			dbc = dbc.Where("del_flag like ?", "%"+pp.DelFlag+"%")
		} else {
			dbc = dbc.Where("del_flag=?", pp.DelFlag)
		}

	}

	if pp.CreateBy != "" {

		if param.FuzzyQuery {
			dbc = dbc.Where("create_by like ?", "%"+pp.CreateBy+"%")
		} else {
			dbc = dbc.Where("create_by=?", pp.CreateBy)
		}

	}

	//if pp.CreateTime != "" {
	//	b,_ := strconv.ParseBool(pp.CreateTime) //ParseBool(str string)(value bool, err error)
	//	dbc = dbc.Where("create_time=?", b)
	//}

	if pp.UpdateBy != "" {

		if param.FuzzyQuery {
			dbc = dbc.Where("update_by like ?", "%"+pp.UpdateBy+"%")
		} else {
			dbc = dbc.Where("update_by=?", pp.UpdateBy)
		}

	}

	//if pp.UpdateTime != "" {
	//	b,_ := strconv.ParseBool(pp.UpdateTime) //ParseBool(str string)(value bool, err error)
	//	dbc = dbc.Where("update_time=?", b)
	//}

	return dbc

}

/*
@title     函数名称:  Demo
@description      :  代码调用示例

@auth      作者:      leijianming@163.com 时间: 2024-01-24 17:15:32
@param     输入参数名: 无
@return    返回参数名: 无
*/
func (daoInst *SysDeptDAO) Demo() {
	//var InstSysDeptDAO = dao.InstSysDeptDAO
	entity, _, _ := InstSysDeptDAO.FindById(3)
	fmt.Println(*entity)

	var qp dto.SysDeptQueryParam
	qp.Param = new(dto.SysDeptParam)
	qp.Ini()

	entities, _ := InstSysDeptDAO.FindByQueryParam(&qp)
	/*for _, v := range *entities {
		if v.Name != nil {
			fmt.Println(v.String())
		}
	}*/
	fmt.Println(len(*entities))
	fmt.Println(qp.String())

	var sentity model.SysDept
	sentity.Ini(false)
	id, _ := InstSysDeptDAO.Insert(&sentity)
	fmt.Println(id)

	ret, _, _ := InstSysDeptDAO.FindById(entity.DeptId)
	fmt.Println(ret.String())

	entity.DeptId = sentity.DeptId
	InstSysDeptDAO.Update(entity)

	InstSysDeptDAO.DeleteById(entity.DeptId)

}
