package dao

/*
   @Title  文件名称: SysDeptDao_test.goService
   @Description 描述: 测试服务SysDeptDao

   @Author  作者: leijianming@163.com  时间(2024-02-02 23:19:40)
   @Update  作者: leijianming@163.com  时间(2024-02-02 23:19:40)

*/

import (
	"fmt"
	"gitee.com/ichub/godbquery/common/base/basedto"
	"gitee.com/ichub/godbquery/common/dbcontent"
	"gitee.com/ichub/godbquery/shop/model"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/suite"

	"testing"
	"time"
)

// 数据库连接
var SysDeptDbinst *gorm.DB

// 服务实例
var entityServiceSysDept SysDeptDAO

type TestSuiteSysDept struct {
	suite.Suite
	sysDeptDao SysDeptDAO
	db         *gorm.DB
}

func TestSuitesSysDept(t *testing.T) {
	suite.Run(t, new(TestSuiteSysDept))
}

/*
@title     函数名称: Setup
@description      : 测试套前置脚本
@auth      作者: leijianming@163.com 时间: 2024-02-02 23:19:40
@param     输入参数名:        无

@return    返回参数名:        无
*/
func SysDeptSetup() {

	fmt.Println("setUp all tests")
	//mysql pg RoachPg MyRoach
	SysDeptDbinst = SysDeptIniDb("RoachPg")

}
func (suite *TestSuiteSysDept) SetupTest() {
	fmt.Println("setUp all tests")
	//mysql pg RoachPg MyRoach
	SysDeptDbinst = SysDeptIniDb("mysql")

}
func (suite *TestSuiteSysDept) SetupSuite() {
	fmt.Println("Suite setup")
}

func (suite *TestSuiteSysDept) TearDownSuite() {
	fmt.Println("Suite teardown")
}

func (suite *TestSuiteSysDept) TearDownTest() {

}

/*
@title     函数名称: Teardown
@description      : 测试套后置脚本
@auth      作者: leijianming@163.com 时间: 2024-02-02 23:19:40
@param     输入参数名:        无

@return    返回参数名:        无
*/
func SysDeptTeardown() {
	fmt.Println("tearDown all tests")
}

/*
@title     函数名称: init
@description      : 初始化(TestMain 不能重复)
@auth      作者    : leijianming@163.com 时间: 2024-02-02 23:19:40
@param     输入参数名:        dbtype string

	取值：mysql, pg, RoachPg

@return    返回参数名:        *gorm.DB
*/
func init() {
	//SysDeptSetup()
}

/*
   @title     函数名称 : TestMain
   @description       : 测试套全流程脚本
   @auth      作者     : leijianming@163.com 时间: 2024-02-02 23:19:40
   @param     输入参数名:        m *testing.M

   @return    返回参数名:        无
*/
/*
func TestMain(m *testing.M) {
	SysDeptSetup()
	defer SysDeptDbinst.Close()

	code := m.Run()

	SysDeptTeardown()
	os.Exit(code)
}
*/

/*
@title     函数名称: IniDb
@description      : 连接数据库
@auth      作者   : leijianming@163.com 时间: 2024-02-02 23:19:40
@param     输入参数名:        dbtype string

	取值：mysql, pg, RoachPg

@return    返回参数名:        *gorm.DB
*/
func SysDeptIniDb(dbtype string) *gorm.DB {

	var dbinst *gorm.DB = dbcontent.IniDbType(dbtype)

	return dbinst
}

/*
@title     函数名称: TestSysDeptSave
@description      : 保存接口  主键%s为nil，0新增，!=nil修改

	Save(entity *basemodel.SysDept) *basedto.IchubResults

@auth      作者    :   leijianming@163.com 时间: 2024-02-02 23:19:40
@param     输入参数名:  t *testing.T
@return    返回参数名:  无
*/
func (suite *TestSuiteSysDept) SysDeptSave() int64 {
	var entity model.SysDept

	entity.Ini(false)
	id, err := entityServiceSysDept.Save(&entity)
	fmt.Println(id)
	if err != nil {
		suite.T().Errorf("save error:" + entity.String())
		return -1
	}
	return entity.DeptId
}

/*
@title     函数名称: TestSysDeptSaveEntity
@description      : 保存接口  主键%s为nil，0新增，!=nil修改

	Save(entity *basemodel.SysDept) *basedto.IchubResults

@auth      作者    :   leijianming@163.com 时间: 2024-02-02 23:19:40
@param     输入参数名:  t *testing.T
@return    返回参数名:  无
*/
func SysDeptSaveEntity(t *testing.T, entity *model.SysDept) int64 {
	//entity.InitPage(false)

	id, err := entityServiceSysDept.Save(entity)
	fmt.Println(id)
	if err != nil {
		t.Errorf("save error:" + err.Error())
		return -1
	}

	return id
}

/*
@title     函数名称: SysDeptCheckJsonResult
@description      : 检查结果返回码正确

@auth      作者    :   leijianming@163.com 时间: 2024-02-02 23:19:40
@param     输入参数名:  t *testing.T,

	result *basedto.IchubResults,
	code int32: 期望的返回码

@return    返回参数名:  无
*/
func SysDeptCheckJsonResult(t *testing.T, result *basedto.JsonResult, code int) {
	if result.Code != code {
		t.Errorf("IchubResults code %d != %d ", result.Code, code)
	}
	fmt.Println(result.String())
}

/*
@title     函数名称: SysDeptUpdateNotNullProps
@description      : 公共函数：更新功能

@auth      作者     :  leijianming@163.com 时间: 2024-02-02 23:19:40
@param     输入参数名:  t *testing.T,

	entity basemodel.SysDept

@return    返回参数名:  pkey int64
*/
func SysDeptUpdateNotNullProps(t *testing.T, entity model.SysDept) {

}

/*
@title     函数名称: Test001_SysDeptSave
@description      : 测试接口-保存记录, 主键%s为nil或者为0新增, !=nil修改

@auth      作者    :   leijianming@163.com 时间: 2024-02-02 23:19:40
@param     输入参数名:  t *testing.T
@return    返回参数名:  无
*/
func (suite *TestSuiteSysDept) Test001_SysDeptSave(t *testing.T) {
	t.Logf("start Save ...")
	pkey := suite.SysDeptSave()
	t.Logf(fmt.Sprintf("success Save ok! pkey = %d", pkey))

	result, found, err := entityServiceSysDept.FindById(pkey)
	fmt.Println(found)
	if err != nil {
		t.Errorf(err.Error())
	}
	t.Logf("result:%s", result)
}

/*
 @title     函数名称: Test002_SysDeptUpdateNotNullProps
 @description      : 测试接口-更新非空字段,条件为主键DeptId

 @auth      作者    :   leijianming@163.com 时间: 2024-02-02 23:19:40
 @param     输入参数名:  t *testing.T
 @return    返回参数名:  无

*/

func Test002_SysDeptUpdateNotNullProps(t *testing.T) {

}

/*
@title     函数名称: Test003_SysDeptDeleteById
@description      : 测试接口-根据主键DeptId删除记录
@auth      作者    :   leijianming@163.com 时间: 2024-02-02 23:19:40
@param     输入参数名:  t *testing.T
@return    返回参数名:  无
*/
func Test003_SysDeptDeleteById(t *testing.T) {
	t.Logf("start DeleteById ...")

}

/*
@title     函数名称: Test004_SysDeptFindById
@description      : 测试接口-根据主键DeptId查找记录
@auth      作者    :   leijianming@163.com 时间: 2024-02-02 23:19:40
@param     输入参数名:  t *testing.T
@return    返回参数名:  无
*/
func Test004_SysDeptFindById(t *testing.T) {
	t.Logf("start Test EsFindId ...")

}

/*
@title     函数名称: Test005_SysDeptFindByIds
@description      : 测试接口-根据主键DeptId 查询多条记录, FindByIds("1,36,39")
@auth      作者    :   leijianming@163.com 时间: 2024-02-02 23:19:40
@param     输入参数名:  t *testing.T
@return    返回参数名:  无
*/
func Test005_SysDeptFindByIds(t *testing.T) {
	t.Logf("start   FindByIds ...")

}

/*
   @title     函数名称: Test006_SysDeptQuery
   @description      : 测试接口-通用查询
   @auth      作者    : leijianming@163.com 时间: 2024-02-02 23:19:40
   @param     输入参数名:        t *testing.T
   @return    返回参数名:        无
*/

func SysDeptStrTime2Int(datetime string) int64 {
	//日期转化为时间戳
	timeLayout := "2006-01-02 15:04:05"  //转化所需模板
	loc, _ := time.LoadLocation("Local") //获取时区
	tmp, _ := time.ParseInLocation(timeLayout, datetime, loc)
	timestamp := tmp.Unix() //转化为时间戳 类型是int64
	return timestamp
}

func (suite *TestSuiteSysDept) Test006_SysDeptQuery(t *testing.T) {
	t.Logf("start  EsQuery ...")

	//fmt.Printf(pageResult.String())

}

//queryParam.DateRanges ["atimestamp"] = []string{"2021/08/15 00:00:00","2022/12/31 00:00:00"}
