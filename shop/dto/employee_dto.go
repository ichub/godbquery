package dto

import (
	"gitee.com/ichub/godbquery/shop/model"
)

/*
   @Title  文件名称: Employeedto.go
   @Description 描述: 服务Employeedto

   @Author  作者: leijianming@163.com  时间(2024-03-26 16:26:57)
   @Update  作者: leijianming@163.com  时间(2024-03-26 16:26:57)

*/

/*
 */
type EmployeeDto struct {
	model.Employee
}

func NewEmployeeDto() *EmployeeDto {
	var m = &EmployeeDto{}
	m.InitProxy(m)
	return m
}

type EmployeeParamsDto struct {
	model.EmployeeParams
}
type EmployeeEntityDto struct {
	model.EmployeeEntity
}

func NewEmployeeEntityDto() *EmployeeEntityDto {
	var m = &EmployeeEntityDto{}
	//m.InitProxy(m)
	return m
}

func NewEmployeeParamsDto() *EmployeeParamsDto {
	var m = &EmployeeParamsDto{}
	//m.InitProxy(m)
	return m
}
