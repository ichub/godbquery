package dto

import (
	"encoding/json"
	basedto "gitee.com/ichub/godbquery/common/base/basedto"
	"gitee.com/ichub/godbquery/shop/model"
)

type ParamsQueryParam struct {
	Current    int    `json:"current"`
	PageSize   int    `json:"page_size"`
	OrderBys   string `json:"order_bys"`
	FuzzyQuery bool   `json:"fuzzy_query"`
	EsQuery    bool   `json:"es_query"`

	Param *ParamsParam `json:"param"`
}

type ParamsParam struct {
	DateRanges   map[string][]int64  `json:"date_ranges,string"`
	IntRanges    map[string][]int64  `json:"int_ranges,string"`
	StringRanges map[string][]string `json:"string_ranges"`
	InRanges     map[string]string   `json:"in_ranges"`

	model.ParamsVo
}

func (param *ParamsQueryParam) Ini() {
	param.Current = 1
	param.PageSize = 20
	//param.OrderBys = new(string)

	param.FuzzyQuery = false
	param.EsQuery = false

	param.Param = new(ParamsParam)

	param.Param.DateRanges = make(map[string][]int64)
	param.Param.IntRanges = make(map[string][]int64)
	param.Param.StringRanges = make(map[string][]string)
	param.Param.InRanges = make(map[string]string)

}

func (param *ParamsQueryParam) ToString() string {
	s, _ := json.MarshalIndent(param, "", "    ")
	return string(s)

}

func (param *ParamsQueryParam) String() string {
	s, _ := json.Marshal(param)
	return string(s)

}

type ParamsJsonResult struct {
	Code int32         `json:"code"`
	Msg  string        `json:"msg"`
	Data *model.Params `json:"data"`
}

func (result *ParamsJsonResult) String() string {
	s, _ := json.Marshal(result)
	return string(s)

}

func (result *ParamsJsonResult) ToString() string {
	s, _ := json.MarshalIndent(result, "", "    ")
	return string(s)

}
func (result *ParamsJsonResult) Success() *ParamsJsonResult {
	result.Code = basedto.CODE_SUCCESS
	result.Msg = "成功"
	return result
}

func (result *ParamsJsonResult) SuccessData(data model.Params) *ParamsJsonResult {
	result.Code = basedto.CODE_SUCCESS
	result.Msg = "成功"
	result.Data = &data
	return result
}
func (result *ParamsJsonResult) SuccessMessage(msg string) *ParamsJsonResult {
	result.Code = basedto.CODE_SUCCESS
	result.Msg = msg

	return result
}

func (result *ParamsJsonResult) Fail() *ParamsJsonResult {
	result.Code = basedto.CODE_FAIL
	result.Msg = "失败"
	return result
}

func (result *ParamsJsonResult) FailMessage(msg string) *ParamsJsonResult {
	result.Code = basedto.CODE_FAIL
	result.Msg = msg
	return result
}

func (result *ParamsJsonResult) FailCodeMsg(code int32, msg string) *ParamsJsonResult {
	result.Code = code
	result.Msg = msg
	return result
}

type ParamsPageResult struct {
	Code int32  `json:"code"`
	Msg  string `json:"msg"`

	Page basedto.PageParam `json:"page"`
	Data []model.Params    `json:"data"`
}

func (result *ParamsPageResult) String() string {
	s, _ := json.Marshal(result)
	return string(s)

}

func (result *ParamsPageResult) ToString() string {
	s, _ := json.MarshalIndent(result, "", "    ")
	return string(s)

}

func (result *ParamsPageResult) SetData(s []model.Params) {
	result.Data = s
}

func (result *ParamsPageResult) GetData() []model.Params {

	return result.Data
}

func (result *ParamsPageResult) Success() *ParamsPageResult {
	result.Code = basedto.CODE_SUCCESS
	result.Msg = "成功"
	return result
}

func (result *ParamsPageResult) SuccessData(data []model.Params) *ParamsPageResult {
	result.Code = basedto.CODE_SUCCESS
	result.Msg = "成功"
	//result.Data = data
	return result
}
func (result *ParamsPageResult) SuccessMessage(msg string) *ParamsPageResult {
	result.Code = basedto.CODE_SUCCESS
	result.Msg = msg
	return result
}

func (result *ParamsPageResult) Fail() *ParamsPageResult {
	result.Code = basedto.CODE_FAIL
	result.Msg = "失败"
	return result
}

func (result *ParamsPageResult) FailMessage(msg string) *ParamsPageResult {
	result.Code = basedto.CODE_FAIL
	result.Msg = msg
	return result
}

func (result *ParamsPageResult) FailCodeMsg(code int32, msg string) *ParamsPageResult {
	result.Code = code
	result.Msg = msg
	return result
}
