package dto

import (
	"encoding/json"
	basedto "gitee.com/ichub/godbquery/common/base/basedto"
	"gitee.com/ichub/godbquery/common/base/baseutils/jsonutils"
	"gitee.com/ichub/godbquery/shop/model"
	"github.com/jinzhu/gorm"
)

type RulesQueryParam struct {
	basedto.QueryParam
	Param *RulesParam `json:"param"`
	model model.Rules
}

func NewRulesQueryParam(param *RulesParam) *RulesQueryParam {
	return &RulesQueryParam{Param: param}
}

type RulesParam struct {
	DateRanges   map[string][]int64  `json:"date_ranges,string"`
	IntRanges    map[string][]int64  `json:"int_ranges,string"`
	StringRanges map[string][]string `json:"string_ranges"`
	InRanges     map[string]string   `json:"in_ranges"`

	model.Rules
}

func (r *RulesQueryParam) buildWhere(dbc *gorm.DB) *gorm.DB {
	if r.model.Rule != "" {
		dbc = dbc.Where("rule_text=?", r.model.Rule)
	}
	return dbc
}

func (param *RulesQueryParam) Ini() {
	param.Current = 1
	param.PageSize = 20
	//param.OrderBys = new(string)

	param.FuzzyQuery = false
	param.EsQuery = false

	param.Param = new(RulesParam)

	param.Param.DateRanges = make(map[string][]int64)
	param.Param.IntRanges = make(map[string][]int64)
	param.Param.StringRanges = make(map[string][]string)
	param.Param.InRanges = make(map[string]string)

}

func (param *RulesQueryParam) ToString() string {
	s, _ := json.MarshalIndent(param, "", "    ")
	return string(s)

}

func (param *RulesQueryParam) String() string {
	s, _ := json.Marshal(param)
	return string(s)

}

type RulesJsonResult struct {
	Code int          `json:"code"`
	Msg  string       `json:"msg"`
	Data *model.Rules `json:"data"`
}

func (result *RulesJsonResult) String() string {
	return jsonutils.ToJsonPretty(result)

}

func (result *RulesJsonResult) ToString() string {
	s, _ := json.MarshalIndent(result, "", "    ")
	return string(s)

}
func (result *RulesJsonResult) Success() *RulesJsonResult {
	result.Code = basedto.CODE_SUCCESS
	result.Msg = "成功"
	return result
}

func (result *RulesJsonResult) SuccessData(data model.Rules) *RulesJsonResult {
	result.Code = basedto.CODE_SUCCESS
	result.Msg = "成功"
	result.Data = &data
	return result
}
func (result *RulesJsonResult) SuccessMessage(msg string) *RulesJsonResult {
	result.Code = basedto.CODE_SUCCESS
	result.Msg = msg

	return result
}

func (result *RulesJsonResult) Fail() *RulesJsonResult {
	result.Code = basedto.CODE_FAIL
	result.Msg = "失败"
	return result
}

func (result *RulesJsonResult) FailMessage(msg string) *RulesJsonResult {
	result.Code = basedto.CODE_FAIL
	result.Msg = msg
	return result
}

func (result *RulesJsonResult) FailCodeMsg(code int, msg string) *RulesJsonResult {
	result.Code = code
	result.Msg = msg
	return result
}

type RulesPageResult struct {
	basedto.BaseEntity
	Code int    `json:"code"`
	Msg  string `json:"msg"`

	Page basedto.PageParam `json:"page"`
	Data []model.Rules     `json:"data"`
}

func NewRulesPageResult() *RulesPageResult {
	return &RulesPageResult{}
}

//func (result *RulesPageResult) ToString() string {
//	//	s, _ := json.MarshalIndent(result, "", "    ")
//	return jsonutils.ToJsonPretty(result) //string(s)
//
//}

func (result *RulesPageResult) SetData(s []model.Rules) {
	result.Data = s
}

func (result *RulesPageResult) GetData() []model.Rules {

	return result.Data
}

func (result *RulesPageResult) Success() *RulesPageResult {
	result.Code = basedto.CODE_SUCCESS
	result.Msg = "成功"
	return result
}

func (result *RulesPageResult) SuccessData(data []model.Rules) *RulesPageResult {
	result.Code = basedto.CODE_SUCCESS
	result.Msg = "成功"
	//result.Data = data
	return result
}
func (result *RulesPageResult) SuccessMessage(msg string) *RulesPageResult {
	result.Code = basedto.CODE_SUCCESS
	result.Msg = msg
	return result
}

func (result *RulesPageResult) Fail() *RulesPageResult {
	result.Code = basedto.CODE_FAIL
	result.Msg = "失败"
	return result
}

func (result *RulesPageResult) FailMessage(msg string) *RulesPageResult {
	result.Code = basedto.CODE_FAIL
	result.Msg = msg
	return result
}

func (result *RulesPageResult) FailCodeMsg(code int, msg string) *RulesPageResult {
	result.Code = code
	result.Msg = msg
	return result
}
