package dto

import (
	"encoding/json"
	"gitee.com/ichub/godbquery/common/base/basedto"
	"gitee.com/ichub/godbquery/shop/model"
)

type SysDeptQueryParam struct {
	Current    int    `json:"current"`
	PageSize   int    `json:"page_size"`
	OrderBys   string `json:"order_bys"`
	FuzzyQuery bool   `json:"fuzzy_query"`
	EsQuery    bool   `json:"es_query"`

	Param *SysDeptParam `json:"param"`
}

type DateInt int64
type BetweenFields struct {
	fieldName string
	start     any
	end       any
	startDate DateInt
	endDate   DateInt
	inValue   string
	eqType    string //= > < >= <=
	eqValue   string
	likeValue string
}

type QueryRange struct {
	DateRanges map[string][]int64 `json:"date_ranges,string"`
	Ranges     map[string][]any   `json:"ranges"`
	InRanges   map[string]string  `json:"in_ranges"`
}
type SysDeptParam struct {
	DateRanges   map[string][]int64  `json:"date_ranges,string"`
	IntRanges    map[string][]int64  `json:"int_ranges,string"`
	StringRanges map[string][]string `json:"string_ranges"`
	InRanges     map[string]string   `json:"in_ranges"`

	model.SysDeptVo
}

func (param *SysDeptQueryParam) Ini() {
	param.Current = 1
	param.PageSize = 20
	//param.OrderBys = new(string)

	param.FuzzyQuery = false
	param.EsQuery = false

	param.Param = new(SysDeptParam)

	param.Param.DateRanges = make(map[string][]int64)
	param.Param.IntRanges = make(map[string][]int64)
	param.Param.StringRanges = make(map[string][]string)
	param.Param.InRanges = make(map[string]string)

}

func (param *SysDeptQueryParam) ToString() string {
	s, _ := json.MarshalIndent(param, "", "    ")
	return string(s)

}

func (param *SysDeptQueryParam) String() string {
	s, _ := json.Marshal(param)
	return string(s)

}

type SysDeptJsonResult struct {
	Code int32          `json:"code"`
	Msg  string         `json:"msg"`
	Data *model.SysDept `json:"data"`
}

func (result *SysDeptJsonResult) String() string {
	s, _ := json.Marshal(result)
	return string(s)

}

func (result *SysDeptJsonResult) ToString() string {
	s, _ := json.MarshalIndent(result, "", "    ")
	return string(s)

}
func (result *SysDeptJsonResult) Success() *SysDeptJsonResult {
	result.Code = basedto.CODE_SUCCESS
	result.Msg = "成功"
	return result
}

func (result *SysDeptJsonResult) SuccessData(data model.SysDept) *SysDeptJsonResult {
	result.Code = basedto.CODE_SUCCESS
	result.Msg = "成功"
	result.Data = &data
	return result
}
func (result *SysDeptJsonResult) SuccessMessage(msg string) *SysDeptJsonResult {
	result.Code = basedto.CODE_SUCCESS
	result.Msg = msg

	return result
}

func (result *SysDeptJsonResult) Fail() *SysDeptJsonResult {
	result.Code = basedto.CODE_FAIL
	result.Msg = "失败"
	return result
}

func (result *SysDeptJsonResult) FailMessage(msg string) *SysDeptJsonResult {
	result.Code = basedto.CODE_FAIL
	result.Msg = msg
	return result
}

func (result *SysDeptJsonResult) FailCodeMsg(code int32, msg string) *SysDeptJsonResult {
	result.Code = code
	result.Msg = msg
	return result
}

type SysDeptPageResult struct {
	Code int32  `json:"code"`
	Msg  string `json:"msg"`

	Page basedto.PageParam `json:"page"`
	Data []model.SysDept   `json:"data"`
}

func (result *SysDeptPageResult) String() string {
	s, _ := json.Marshal(result)
	return string(s)

}

func (result *SysDeptPageResult) ToString() string {
	s, _ := json.MarshalIndent(result, "", "    ")
	return string(s)

}

func (result *SysDeptPageResult) SetData(s []model.SysDept) {
	result.Data = s
}

func (result *SysDeptPageResult) GetData() []model.SysDept {

	return result.Data
}

func (result *SysDeptPageResult) Success() *SysDeptPageResult {
	result.Code = basedto.CODE_SUCCESS
	result.Msg = "成功"
	return result
}

func (result *SysDeptPageResult) SuccessData(data []model.SysDept) *SysDeptPageResult {
	result.Code = basedto.CODE_SUCCESS
	result.Msg = "成功"
	//result.Data = data
	return result
}
func (result *SysDeptPageResult) SuccessMessage(msg string) *SysDeptPageResult {
	result.Code = basedto.CODE_SUCCESS
	result.Msg = msg
	return result
}

func (result *SysDeptPageResult) Fail() *SysDeptPageResult {
	result.Code = basedto.CODE_FAIL
	result.Msg = "失败"
	return result
}

func (result *SysDeptPageResult) FailMessage(msg string) *SysDeptPageResult {
	result.Code = basedto.CODE_FAIL
	result.Msg = msg
	return result
}

func (result *SysDeptPageResult) FailCodeMsg(code int32, msg string) *SysDeptPageResult {
	result.Code = code
	result.Msg = msg
	return result
}
