package service

/*
   @Title  文件名称 : Employee_service.go
   @Description 描述: 服务EmployeeService

   @Author  作者: leijianming@163.com  时间: 2024-03-26 19:12:42
   @Update  作者: leijianming@163.com  时间: 2024-03-26 19:12:42

*/

import (
	"fmt"
	"gitee.com/ichub/godbquery/common/base/basedto"
	"gitee.com/ichub/godbquery/common/base/pagedto"
	"gitee.com/ichub/godbquery/shop/dao"
	"gitee.com/ichub/godbquery/shop/model"
)

// SERVICE层服务实例变量
var InstEmployeeService = new(EmployeeService)

// SERVICE层服务结构体
type EmployeeService struct {
	Dao dao.EmployeeDAO
}

func (this *EmployeeService) FindData(result *basedto.IchubResult) *model.Employee {
	return result.Data.(*model.Employee)
}

func (this *EmployeeService) FindDataList(result *pagedto.IchubPageResult) *[]model.Employee {
	return result.Data.(*[]model.Employee)
}

/*
@title     函数名称:  Query
@description      :  通用查询
@auth      作者:      leijianming@163.com时间: 2024-03-26 12:46:24
@param     输入参数名: param * pagedto.IchubPageRequest
@return    返回参数名: * pagedto.IchubPageResult
*/
func (this *EmployeeService) Query(param *pagedto.IchubPageRequest) *pagedto.IchubPageResult {

	return this.Dao.Query(param) //	this.fills(entities)
}

/*
   @title     函数名称 :  Count
   @description       :  通用查询计数
   @auth      作者     : leijianming@163.com时间: 2024-03-26 12:46:24
   @param     输入参数名: param * pagedto.IchubPageRequest
   @return    返回参数名: int  ,error
*/

func (this *EmployeeService) Count(param *pagedto.IchubPageRequest) (int, error) {
	return this.Dao.Count(param)
}

/*
@title     函数名称: fills
@description      : 填充关联子表信息。
@auth      作者   : leijianming@163.com 时间: 2024-03-26 19:12:42
@param     输入参数名: entity *model.Employee
@return    返回参数名: 无
*/
func (this *EmployeeService) fills(entities *[]model.Employee) {

	for index := range *entities {

		this.fill(&(*entities)[index])
	}

}

/*
@title     函数名称: fill
@description      : 填充关联子表信息。
@auth      作者   : leijianming@163.com 时间: 2024-03-26 19:12:42
@param     输入参数名: entity *model.Employee
@return    返回参数名: 无
*/
func (this *EmployeeService) fill(entity *model.Employee) {

}

/*
@title     函数名称: FindById(Id int32)
@description      : 根据主键查询记录
@auth      作者:     leijianming@163.com 时间: 2024-03-26 19:12:42
@param     输入参数名:Id int32
@return    返回参数名:*basedto.IchubResult
*/
func (this *EmployeeService) FindById(Id int32) *basedto.IchubResult {

	var result = basedto.NewIchubResult()
	entity, found, err := this.Dao.FindById(Id)

	if err != nil || !found {
		result.CodeMsg(basedto.CODE_NOFOUND_RECORD, err.Error())
		return result
	}

	result.Data = entity
	return result

}

/*
@title     函数名称: FindByIds(pks string)
@description      :

	根据主键Id  查询多条记录
	例子： FindByIds("1,36,39")

@auth      作者:     leijianming@163.com 时间: 2024-03-26 19:12:42
@param     输入参数名:Id int32
@return    返回参数名:*pagedto.IchubPageResult
*/
func (this *EmployeeService) FindByIds(pks string) *pagedto.IchubPageResult {
	var result = pagedto.NewIchubPageResult()
	entities, err := this.Dao.FindByIds(pks)
	if err != nil {
		return result.FailMsg(err.Error())
	}

	result.Data = entities
	return result
}

/*
@title     函数名称: DeleteById
@description      : 根据主键软删除。
@auth      作者   : leijianming@163.com 时间: 2024-03-26 19:12:42
@param     输入参数名: Id int32
@return    返回参数名: *basedto.IchubResult
*/
func (this *EmployeeService) DeleteById(Id int32) *basedto.IchubResult {
	err := this.Dao.DeleteById(Id)
	var result = basedto.NewIchubResult()
	if err != nil {
		return result.FailMessage(err.Error())

	}

	return result
}

/*
@title     函数名称: Save
@description      : 主键%s为nil,0新增; !=nil修改。
@auth      作者   : leijianming@163.com 时间: 2024-03-26 19:12:42
@param     输入参数名: entity *model.Employee
@return    返回参数名: *basedto.IchubResult
*/
func (this *EmployeeService) Save(entity *model.Employee) *basedto.IchubResult {

	var e error
	if entity.Id == 0 {
		_, e = this.Dao.Insert(entity)
	} else {
		_, e = this.Dao.Update(entity)
	}
	var result = basedto.NewIchubResult()
	if e != nil {
		return result.FailMessage(e.Error())
	}

	result.Data = fmt.Sprintf("%d", entity.Id)
	return result
}

/*
@title     函数名称: UpdateNotNull
@description      : 更新非空字段
@auth      作者   : leijianming@163.com 时间: 2024-03-26 19:12:42
@param     输入参数名: entity *model.Employee
@return    返回参数名: *basedto.IchubResult
*/
func (this *EmployeeService) UpdateNotNull(Id int32, entity map[string]interface{}) *basedto.IchubResult {

	var result = basedto.NewIchubResult()
	return result

}

// End of  EmployeeService
