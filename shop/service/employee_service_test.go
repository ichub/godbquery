package service

/*
   @Title  文件名称: TestEmployeeServiceSuite.go
   @Description 描述: 测试服务TestEmployeeServiceSuite

   @Author  作者: leijianming@163.com 时间: 2024-03-26 19:12:42
   @Update  作者: leijianming@163.com 时间: 2024-03-26 19:12:42

*/

import (
	"fmt"
	"gitee.com/ichub/godbquery/common/base/basedao"
	"gitee.com/ichub/godbquery/common/base/basedto"
	"gitee.com/ichub/godbquery/common/base/pagedto"
	"gitee.com/ichub/godbquery/shop/model"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"strings"
	"testing"
)

type TestEmployeeServiceSuite struct {
	suite.Suite
	basedao.BaseDao
	service EmployeeService
}

func TestEmployeeServiceSuites(t *testing.T) {
	suite.Run(t, new(TestEmployeeServiceSuite))
}

/*
@title     函数名称: Setup
@description      : 测试套前置脚本
@auth      作者: leijianming@163.com
@date      时间: 2024-03-26 19:12:42
@param     输入参数名:        无
@return    返回参数名:        无
*/
func (this *TestEmployeeServiceSuite) SuiteSetupTest() {

	logrus.Info("setUp all tests")
	// mysql postgres cockroach
}

/*
@title           : 测试套后置脚本
@auth      作者: leijianming@163.com
@description 函数名称: Teardown
@date      时间: 2024-03-26 19:12:42
@param     输入参数名:        无
@return    返回参数名:        无
*/
func (this *TestEmployeeServiceSuite) SuiteTearDownTest() {
	logrus.Info("tearDown all tests")
}

/*
@title     函数名称: Save
@description      : 保存接口
@auth      作者    :  leijianming@163.com
@date      时间    :  2024-03-26 19:12:42
@param     输入参数名:  t *testing.T
@return    返回参数名:  无
*/
func (this *TestEmployeeServiceSuite) Save() int32 {
	var entity model.Employee

	result := this.service.Save(&entity)
	logrus.Info(result.String())
	if result.Code != 200 {
		logrus.Errorf("save error: %s", result.Msg)
		return -1
	}
	return entity.Id
}

/*
@title     函数名称: SaveEntity
@description : 保存接口
@auth      作者    : leijianming@163.com
@date      时间    : 2024-03-26 19:12:42
@param     输入参数名: 无
@return    返回参数名: 无
*/
func (this *TestEmployeeServiceSuite) SaveEntity(entity *model.Employee) int32 {

	result := this.service.Save(entity)
	logrus.Info(result)
	if result.Code != 200 {
		logrus.Errorf("save error: %s", result.Msg)
		return -1
	}
	return entity.Id
}

/*
@title     函数名称: UpdateNotNull
@description      : 公共函数：更新功能
@auth      作者     :  leijianming@163.com
@时间: 2024-03-26 19:12:42
@param     输入参数名:  model.Employee
@return    返回参数名:  pkey int32
*/
func (this *TestEmployeeServiceSuite) UpdateNotNull(entity *model.Employee) int32 {

	return 0

}

/*
@title     函数名称: Test001_Save
@description      : 测试接口-保存记录, 主键%s为nil或者为0新增, !=nil修改

@auth      作者    : leijianming@163.com
@date      时间    : 2024-03-26 19:12:42
@param     输入参数名:
@return    返回参数名:  无
*/
func (this *TestEmployeeServiceSuite) Test001_Save() {
	logrus.Info("start Save ...")
	pkey := this.Save()
	logrus.Infof(fmt.Sprintf("success Save ok! pkey = %d", pkey))

	result := this.service.FindById(pkey)
	logrus.Infof("result:%s", result)
}

/*
@title     函数名称: Test002_UpdateNotNull
@description      : 测试接口-更新非空字段,条件为主键Id

@auth      作者    :   leijianming@163.com
@时间: 2024-03-26 19:12:42
@param     输入参数名:
@return    返回参数名:  无
*/
func (this *TestEmployeeServiceSuite) Test002_UpdateNotNull() {
	logrus.Info("start UpdateNotNull ...")

	entity := &model.Employee{}
	pkey := this.SaveEntity(entity)
	result := this.service.FindById(pkey)

	if result.Code != 200 {
		logrus.Println(result.String())
		logrus.Errorf(result.Msg)
	}

	entity = this.service.FindData(result)
	entity.Id = pkey
	// entity.WorkId = fmt.Sprintf("Name%d", time.Now().Second())
	pkey = this.UpdateNotNull(entity)
	logrus.Infof("%d", pkey)

	findResult := this.service.FindById(pkey)
	var retenity = this.service.FindData(findResult)
	//if findResult.Code != 200 || retenity.WorkId != entity.WorkId {
	//	logrus.Errorf("findResult:=" + findResult.String())
	//	logrus.Errorf("UpdateNotNullProp faild! not eq " + entity.WorkId)
	//}
	logrus.Infof("success UpdateNotNull %s", retenity.String())
	//logrus.Infof("success UpdateNotNull pkey = %d, entity.WorkId=%s...", pkey, entity.WorkId)

}

/*
@title     函数名称: Test003_DeleteById
@description      : 测试接口-根据主键Id删除记录
@auth      作者    :   leijianming@163.com
@时间: 2024-03-26 19:12:42
@param     输入参数名:
@return    返回参数名:  无
*/
func (this *TestEmployeeServiceSuite) Test003_DeleteById() {
	logrus.Info("start DeleteById ...")
	pkey := this.Save()
	result := this.service.FindById(pkey)
	if result.Code != 200 {
		logrus.Errorf("DeleteById no found id:%d!", pkey)
	}
	baseresult := this.service.DeleteById(pkey)
	if baseresult.Code != 200 {
		logrus.Errorf(baseresult.Msg)
	} else {
		logrus.Infof(baseresult.Msg)
	}
	result = this.service.FindById(pkey)

	if result.Code != basedto.CODE_NOFOUND_RECORD {
		logrus.Errorf("Fail to delete!")
	}
	logrus.Infof("success DeleteById %d...", pkey)

}

/*
@title     函数名称: Test004_FindById
@description      : 测试接口-根据主键Id查找记录
@auth      作者    : leijianming@163.com
@date      时间    : 2024-03-26 19:12:42
@param     输入参数名:
@return    返回参数名:  无
*/
func (this *TestEmployeeServiceSuite) Test004_FindById() {

	logrus.Info("start Test FindById ...")
	entity := model.NewEmployee()
	//	entity.DepartmentId = 1
	pkey := this.SaveEntity(entity)

	logrus.Infof("Test save pkey is : %d", pkey)
	result := this.service.FindById(pkey)
	if result.Code != 200 {
		logrus.Errorf(result.String())
	}
	logrus.Info(result.ToString())

	if this.service.FindData(result).Id != pkey {
		logrus.Errorf("Test FindById no found %d!", pkey)
	}

}

/*
@title     函数名称: Test005_FindByIds
@description      : 测试接口-根据主键查询多条记录, FindByIds("1,36,39")
@auth      作者    : leijianming@163.com
@date      时间    : 2024-03-26 19:12:42
@param     输入参数名:
@return    返回参数名:  无
*/
func (this *TestEmployeeServiceSuite) Test005_FindByIds() {
	logrus.Infof("start FindByIds ...")
	var pkeys []string
	var entity = model.NewEmployee()
	//entity.DepartmentId = 1
	pkey := this.SaveEntity(entity)
	pkeys = append(pkeys, fmt.Sprintf("%d", pkey))

	entity = model.NewEmployee()
	//entity.DepartmentId = 1
	pkey = this.SaveEntity(entity)
	pkeys = append(pkeys, fmt.Sprintf("%d", pkey))

	result := this.service.FindByIds(strings.Join(pkeys, ","))
	logrus.Info(result.ToPrettyString())
	if result.Total != 2 {
		logrus.Errorf("no found!")
	}
	logrus.Infof("success   FindByIds( %s ) OK! ", strings.Join(pkeys, ","))
}

/*
@title     函数名称: Test006_Query
@description      : 测试接口-通用查询
@auth      作者    :   leijianming@163.com
@时间: 2024-03-26 19:12:42
@param     输入参数名:
@return    返回参数名:  无
*/
func (this *TestEmployeeServiceSuite) Test006_Query() {
	logrus.Info("start  Query ...")

	entity := model.NewEmployee()
	entity.DepartmentId = 1
	pkey := this.SaveEntity(entity)
	pageRequest := pagedto.NewPageRequest(3, 1)
	pageRequest.Eq("Id", pkey)

	logrus.Infof("%d %s", pkey, pageRequest.ToString())

	pageResult := this.service.Query(pageRequest)
	logrus.Info(pageResult)
	if pageResult.Total != 1 {
		logrus.Errorf("未找到记录！")
	}

	logrus.Info(pageResult)
	logrus.Infof("Query total = %d!", pageResult.Total)

}

/*
@title     函数名称: Test007_JoinTables
@description      : 多表关联实例
@auth      作者    : leijianming@163.com
@时间: 2024-03-26 19:12:42
@param     输入参数名:
@return    返回参数名:       无
*/
func (this *TestEmployeeServiceSuite) Test007_JoinTables() {
	/* var ms []ResultEmployee
		db := this.GetDB().Table("Employee").Select("Employee.id,Employee.name")
		db = db.Joins("JOIN department on department.id = Employee.departmentId")
		db = db.Order("id desc ").Limit(5).Find(&ms)
		db = this.GetDB().Table("Employee").Select("Employee.*").Find(&ms)
	    logrus.Info(db.Error +" " + db.RecordNotFound())

		if db.Error != nil {
	        t.Errorf(db.Error.Error())
	    }
		for _, v := range ms {
			fmt.Println(v.String())
		}
	*/
}
